#ifndef __square_wave_test_h__
#define __square_wave_test_h__
#include <stdio.h>
#include "hk32f0301m.h"
#include "sw_tools.h"
#include "square_wave_control.h"

void test_status(SW_CONTROL_S *pControl);

#endif
