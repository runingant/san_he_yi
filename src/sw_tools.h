#ifndef __sw_tools_h__
#define __sw_tools_h__

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <math.h>

/*=======================================================================
          打印debug参数定义
===================================================================-======*/
#define DEBUG_LOG_NUM          (50)   //打印数量

#define MARK_NULL              (0)   //不打印任何东西
#define RECORD_SW_PID          (1)   //打印PID记录的数据
#define RECORD_SPEED_CALC      (2)   //打印速度计算的数据
#define RECORD_HALL_IRQ        (3)   //中断的数据
#define RECORD_HALL            (4)   //打印HALL信息
#define RECORD_CURRENT         (5)   //电流信息
#define READ_HALL              (6)   //读取HALL值
#define MEASURE_HALL           (7)   //标定霍尔
#define RECORD_PRINT_NOW       (100) //实时打印
#define MARK                   MARK_NULL

/*=======================================================================
          时间结构定义
=========================================================================*/
typedef struct system_time_t
{
	uint32_t sec;
	uint32_t ms;
}SYSTEM_TIME_T;


/*=======================================================================
          时间测量
=========================================================================*/
typedef struct measure_t
{
	uint16_t start;
	uint16_t stop;
	uint16_t nowCounter;
	uint16_t maxCounter;
	uint16_t maxCounterStart;
	uint16_t maxCounterStop;
}MEASURE_T;

/*=======================================================================
          打印定义
=========================================================================*/
//#define dbg(format, args...)   printf("[%s/%s](%d)" format, __FILE__,__FUNCTION__,__LINE__, ##args)
//#define dbg2(string...)\
//													do{\
//														if(get_record_log_level() != 0)\
//														{\
//															printf("%s(%d)[%s]: ", __FILE__, __LINE__, __FUNCTION__);\
//															printf(string);\
//															printf("\n");\
//														}\
//													}while(0)

/*=======================================================================
          函数声明
=========================================================================*/
         void update_systime(uint32_t nMs);//更新系统时间，更新nMs
SYSTEM_TIME_T get_systime(void);//获取系统时间
     uint32_t compare_systime_ms(SYSTEM_TIME_T *pTime);//比较时间

void debug_save_log(int32_t a, int32_t b, int32_t c, int32_t d); //存储LOG
void debug_print_log(void); //打印LOG

void delay_ms(uint16_t m);
void delay_01ms(uint16_t m);


#endif
