#include "sw_tools.h"
#include "square_wave_control.h"
#include "application.h"

extern int sample_task(void);

/*=======================================================================
          宏/结构定义
=========================================================================*/
#define KEY_CLEAR_CNTS     3      //按键消抖次数
#define KEY_CONFIRM_CNTS   10     //确认按键次数（10ms周期*20次 = 200ms）
#define KEY_MAX_CNTS       100    //按键限制次数


/* 电量 */
enum
{
  POWER_INVALID=0,
  POWER_LOW,
  POWER_MIDDLE,
  POWER_HIGH,
};


/* LED灯颜色 */
typedef enum color_e
{
  RED=0,
  YELLOW,
  GREEN,
}COLOR_E;

/* 运行状态 */
typedef enum runstate_e
{
  POWERON_STATE = 0,  //上电状态
  NOMAL_STATE,        //正常状态
  CHARGE_STATE,       //充电状态
  POWEROFF_STATE,     //关机
}RUNSTATE_E;

/* 按键检测结构 */
typedef struct key
{
  uint8_t state;      //按键状态(0-未按下，1-已按下, 2-按下后松开, 3-松开已经处理完毕)
  uint8_t pressCnt;   //按下计数器
  uint8_t releaseCnt; //松开计数器
}KEY_T;

/* app结构 */
typedef struct applitcation
{
  RUNSTATE_E runState;      //状态（0-上电阶段、1-正常工作阶段、2-充电阶段）
  uint8_t    powerOnState;  //上电状态（1-上电状态，0-正常状态）
  uint8_t    motorLevel;    //电机档位
  uint8_t    powerLevel;    //电源档位（0-电量低、1-电量中等、2-电量足）
  uint8_t    charging;      //充电中标志
  KEY_T      key;           //按键1 - 开关机/档位键
  KEY_T      usbDect;       //按键2 - USB插入检测
}APP_T;


/*=======================================================================
          声明
=========================================================================*/
void scan_key_module(KEY_T *pKey, uint8_t keyValue);
void show_led_module(uint8_t mode, uint8_t times);
void power_module(void);
void motor_control_module(void);
uint8_t onoff_detect_module(uint8_t ch, uint16_t ms);

void power_on_process(void);   //上电阶段
void nomal_process(void);      //正常处理阶段
void charging_process(void);   //充电阶段
void poweroff_process(void);

/*=======================================================================
          变量定义
=========================================================================*/
APP_T gApplication; 


/*=======================================================================
          模块
=========================================================================*/

/**********************************************************************
 * @bref   休眠函数
 * @param  
 * @note   设置两个唤醒源，WAKEUP1和WAKEUP10
           休眠时进入StandBy模式，功耗<10uA
           StandBy模式唤醒后进入EXTI0_1_IRQHandler、EXTI0_15_IRQHandler
**********************************************************************/
#define PWR_CSR2 (uint32_t *)(PWR_BASE + 0x30)  //因为航顺芯片没有定义，直接操作WAKEUP10的寄存器地址
void sleep(void)
{
  CLOSE_HALL_POWER;  //关闭HALL电源
  NVIC_EnableIRQ(EXTI0_1_IRQn);  //打开KEY中断
  NVIC_EnableIRQ(EXTI4_15_IRQn);  //打开USB_DECT中断
  
  PWR_WakeUpPinCmd(PWR_WakeUpPin_1, ENABLE);  //打开WAKEUP1
  *PWR_CSR2 |= (1<<10);                       //打开WAKEUP10
  PWR_ClearFlag(PWR_FLAG_WU);
  PWR_EnterSTANDBYMode();
}

/**********************************************************************
 * @bref   按键扫描模块
 * @param  
 * @note   本函数快速定时执行（10ms执行周期），主要功能：
           1.扫描按键，自带消抖功能
           2.在按键发生、消失时都需要输出信息（更新全局变量）
           通过pkey->state对外传递按键信息
           1)上电pkey-> = 0
           2)首次检测到按键，pkey->state = 1
           3)检测到按键抬起，pkey->state = 2
           
 使用例程：scan_key_module(&gApplication.key, KEY_PRESS);
 
**********************************************************************/
void scan_key_module(KEY_T *pKey, uint8_t keyValue)
{
  if(keyValue == 0)
  {
    if(pKey->releaseCnt < KEY_MAX_CNTS) //限幅
    {
      pKey->releaseCnt++;
    }

    if(pKey->releaseCnt >= KEY_CLEAR_CNTS)  //消抖,多次释放后才清除PressCnt
    {
      pKey->pressCnt = 0;
    }
    
    /* 按键已按下状态处理 */
    if(pKey->state == 1)
    {
      if(pKey->releaseCnt >= KEY_CONFIRM_CNTS)
      {
        //pKey->state = (pKey->state == 1)?2:0; //确认完全释放后，按键状态=0   
        if(pKey->state == 1)
        {
          pKey->state = 2;
        }
      }
    }
  }
  else
  {
    if(pKey->pressCnt < KEY_MAX_CNTS)
    {
      pKey->pressCnt++;
    }

    if(pKey->pressCnt >= KEY_CLEAR_CNTS)
    {
      pKey->releaseCnt = 0;
    }
    
    /* 按键未按下状态处理 */
    if(pKey->state != 1)
    {
      if((pKey->pressCnt >= KEY_CONFIRM_CNTS) && (pKey->state!=1))
      {
        pKey->state = 1;         //确认完全释放后，按键状态=1
      }
    }
  }
}

/**********************************************************************
 * @bref   流水灯模块
 * @param  mode - 流水灯模式
                =0，从左往右流水灯
                =1，从右往左流水灯
           times - 流水次数(0-10）
 * @note   阻塞完成流水灯过程
**********************************************************************/
#define BLINK_DELAY_TIME   2000
void show_led_module(uint8_t mode, uint8_t times)
{
  while(times--)
  {
    if(mode==0)
    {
      LED1_ON;LED4_OFF;delay_01ms(BLINK_DELAY_TIME);
      LED2_ON;LED1_OFF;delay_01ms(BLINK_DELAY_TIME);
      LED3_ON;LED2_OFF;delay_01ms(BLINK_DELAY_TIME);
      LED4_ON;LED3_OFF;delay_01ms(BLINK_DELAY_TIME);
    }
    else
    {
      LED4_ON;LED1_OFF;delay_01ms(BLINK_DELAY_TIME);
      LED3_ON;LED4_OFF;delay_01ms(BLINK_DELAY_TIME);
      LED2_ON;LED3_OFF;delay_01ms(BLINK_DELAY_TIME);
      LED1_ON;LED2_OFF;delay_01ms(BLINK_DELAY_TIME);
    }
  }
  LED12345_OFF;
}



void vbate_charge_detect(void)
{
  static int state = 0;
  static float s_lastVoltage = 0;
  static SYSTEM_TIME_T s_shutDowmTime;
  float nowVoltage = 0;
  
  if(gApplication.usbDect.state != 1)
  {
    state = 0;
    gApplication.charging = 0;
    return;
  }
  else
  {
    if(USB_STAT_CHARGING)
    {
      gApplication.charging = 1;
    }
    else
    {
      gApplication.charging = 0;
    }
  }
}

/**********************************************************************
 * @bref   电量模块
 * @param  
 * @note   功能
          1.根据电压判断电量
          2.根据电压和充电管脚判断充电状态
**********************************************************************/
void power_module(void)
{
  /* 判断电量 */
  switch(gApplication.powerLevel)
  {
    case POWER_INVALID: /* 无效状态 */
      if(GET_VBAT_VOLTAGE() >= (VBAT_VOLTAGE_GREEN))
      {
        gApplication.powerLevel = POWER_HIGH;
      }
      else if(GET_VBAT_VOLTAGE() >= (VBAT_VOLTAGE_YELLOW))
      {
        gApplication.powerLevel = POWER_MIDDLE;
      }
      else
      {
        gApplication.powerLevel = POWER_LOW;
      }
      break;
    
    case POWER_LOW:/* 电量低 */
      if(GET_VBAT_VOLTAGE() >= (VBAT_VOLTAGE_YELLOW))
      {
        gApplication.powerLevel = POWER_MIDDLE;
      }
      break;
    
    case POWER_MIDDLE:/* 电量一般 */
      if(GET_VBAT_VOLTAGE() <= VBAT_VOLTAGE_YELLOW-VOLTAGE_OFFSET) 
      {
        gApplication.powerLevel = POWER_LOW;
      }
      else if(GET_VBAT_VOLTAGE() >= VBAT_VOLTAGE_GREEN)
      {
        gApplication.powerLevel = POWER_HIGH;
      }
      break;
    
    case POWER_HIGH:/* 电量足 */
      if(GET_VBAT_VOLTAGE() <= (VBAT_VOLTAGE_YELLOW-VOLTAGE_OFFSET))
      {
        gApplication.powerLevel = POWER_MIDDLE;
      }
      break;
    
    default:
      break;
  }
}

/**********************************************************************
 * @bref   开关机检测模块
 * @param  
 * @return ret - 返回值(0-开机中，1-放弃开机，2-开机成功）
 * @note   
**********************************************************************/
#define POWER_ON_FAIL       0
#define POWER_ON_ING        1
#define POWER_ON_SUCEESS    2

uint8_t onoff_detect_module(uint8_t ch, uint16_t ms)
{
  static uint8_t lastch = 0;
  static uint8_t state = 0;
  static SYSTEM_TIME_T s_shutDowmTime;
  KEY_T *pKey = &gApplication.key;
  
  switch(state)
  {
    case POWER_ON_FAIL:
      if(pKey->state == 1)
      {
        lastch = ch;                     //锁定通道ch
        state = POWER_ON_ING;
        s_shutDowmTime = get_systime();  //检测到按键按下，开始定时
      }
      break;
    
    case POWER_ON_ING:
      if(pKey->state != 1)
      {
        state = POWER_ON_FAIL;           //按键已经抬起
      }
      else if(compare_systime_ms(&s_shutDowmTime) >= ms) 
      {
        state = POWER_ON_SUCEESS;        //按键长按有效    
      }
	  if(!ch)	//开机短按有效，长按有效的话这里就不需要了
      {
        state = POWER_ON_SUCEESS;         
      }
      else
      {}
      break;
    
    case POWER_ON_SUCEESS:
      if(pKey->state != 1)
      {
        state = POWER_ON_FAIL;           //按键已经抬起
      }
      break;

    default:break;
  }
  
  
  if(lastch != ch)
  {
    return POWER_ON_FAIL;    //通道数不一致,则直接返回错误
  }
  else
  {
    return state;
  }
}

/**********************************************************************
 * @bref   电机档位控制模块
 * @param  
 * @note   
**********************************************************************/
void motor_control_module(void)
{
  KEY_T *pKey = &gApplication.key;
  
  if(pKey->state == 2)      //按键抬起之后动作
  {
    if(gApplication.powerOnState == 0)  //首次上电的按键不处理
    {
      /* 档位循环累加 */
      gApplication.motorLevel++;
      if(gApplication.motorLevel > 4)
      {
        gApplication.motorLevel = 1;
      }
      
      /* 不同状态下 */
      switch(gApplication.motorLevel)
      {
        case 1:
          LED1_ON;
          LED2_OFF;LED3_OFF;LED4_OFF;
          SET_MOTOR_LEVEL1_SPEED();
          break;
        
        case 2:
          LED2_ON;
          LED1_OFF;LED3_OFF;LED4_OFF;
          SET_MOTOR_LEVEL2_SPEED();
          break;
        
        case 3:
          LED3_ON;
          LED1_OFF;LED2_OFF;LED4_OFF;
          SET_MOTOR_LEVEL3_SPEED();
          break;
        
        case 4:
          LED4_ON;
          LED1_OFF;LED2_OFF;LED3_OFF;
          SET_MOTOR_LEVEL4_SPEED();
          break;
        
        default:break;
      }
    }
    else
    {
      gApplication.powerOnState = 0; //上电完毕
    }
    pKey->state = 3;  //按键已经处理完毕
  }
}



static inline void light_led(void)
{
  if(gApplication.powerLevel == POWER_LOW)          LIGHT_RED();
  else if(gApplication.powerLevel == POWER_MIDDLE)  LIGHT_YELLOW();
  else if(gApplication.powerLevel == POWER_HIGH)    LIGHT_GREEN();
  else {}
}
/**********************************************************************
 * @bref   led模块
 * @param  
 * @note   功能
           1.实现灯的控制，根据控制状态执行常亮、常灭、闪烁灯动作
**********************************************************************/
void led_module(void)
{
  static uint8_t lastState = 0;
  static SYSTEM_TIME_T s_shutDowmTime = {0};
  
  /* 状态改变立即获取最新一次时间 */
  if(lastState != gApplication.charging)
  {
    s_shutDowmTime = get_systime();
    lastState = gApplication.charging;
  }
    
  /* 充电状态灯闪烁,正常状态灯常亮 */
  if(gApplication.charging)
  {
    if(compare_systime_ms(&s_shutDowmTime) >= 800)       //400-800ms灯灭
    {
      DARK_RED_GREEN();
      s_shutDowmTime = get_systime();                    //重新获取时间，开始新定时
    }
    else if(compare_systime_ms(&s_shutDowmTime) >= 400)  //0-400ms灯亮
    {
      light_led();
    }
    else
    {}
  }
  else if((gApplication.runState != POWERON_STATE) && (gApplication.runState != POWEROFF_STATE))        //上电状态不亮灯
  {
    light_led();
  }
}

/*=======================================================================
          主应用任务
=========================================================================*/

/**********************************************************************
 * @bref   驱动任务 【执行周期10ms】
 * @param  
 * @note   功能
           1.按键检测
           2.USB充电检测
**********************************************************************/
void driver_task(void)
{
  scan_key_module(&gApplication.key, KEY_PRESS);
  scan_key_module(&gApplication.usbDect, USB_DECT_PRESS); 
  led_module();
  vbate_charge_detect();
}
 
/* 主状态机切换 */
static inline void switch_runstate(RUNSTATE_E state)
{
  printf("sw=%d\r\n",state);
  gApplication.runState = state;
}


/**********************************************************************
 * @bref   应用初始化
 * @param  
 * @note   功能
           1.初始化应用层状态机和控制设置
           2.初始化按键检测，确保上电应用层读取到正确的按键结果
**********************************************************************/
void application_init(void)
{
  uint16_t check_times = 2*KEY_CONFIRM_CNTS;  
  
  printf("power on\r\n");
  /* 上电设置默认状态 */
  switch_runstate(POWERON_STATE);
  
  /* 关闭输出 */
  SET_MOTOR_CLOSE();
  
  /* 多次运行按键检测(强制滤波),获取准确的按键信息,避免按键唤醒后滤波过程中(此时识别为未按下)的误操作 */
  while(check_times--)
  {
    scan_key_module(&gApplication.key,     KEY_PRESS);
    scan_key_module(&gApplication.usbDect, USB_DECT_PRESS);
  }
}

/**********************************************************************
 * @bref   应用任务
 * @param  
 * @note   功能
        1.系统控制主状态机，上电流程、正常控制流程、充电流程
**********************************************************************/
void application_task(void)
{
  switch(gApplication.runState)
  {
    case POWERON_STATE:
      power_on_process(); 
      break;
    
    case NOMAL_STATE:
      nomal_process();
      break;
    
    case CHARGE_STATE:
      charging_process();
      break;
    
    case POWEROFF_STATE:
      poweroff_process();
      break;
    
    default:break;
  }
  IWDG_ReloadCounter(); //喂狗
}


/**********************************************************************
 * @bref   上电状态
 * @param  
 * @note   功能
           1.开机检测
           2.电量显示
           3.休眠
           4.充电检测
**********************************************************************/
static void power_on_process(void)
{
  uint8_t onOffState = onoff_detect_module(0, 100);
  
  /* 开机过程中插入USB充电 */
  if(gApplication.usbDect.state == 1) 
  {
   printf("[poweron]usb detected, switch to ");
   switch_runstate(CHARGE_STATE);
  }
  else if(sample_task())
  { 
  /* 按键开机/唤醒 */
     if(onOffState == POWER_ON_SUCEESS)
     {
       printf("[poweron]power on ok, switch to ");
       OPEN_HALL_POWER; 
       /* 处理开机时闪烁1次流水灯 */
       show_led_module(0, 1);  
       gApplication.powerOnState = 1;//标记上电状态，需要特殊处理
       switch_runstate(NOMAL_STATE);
     }
     else if(onOffState == POWER_ON_FAIL)
     {
       printf("[poweron]key fail, switch to ");
       gApplication.motorLevel = 0;
       SET_MOTOR_CLOSE();
       switch_runstate(POWERON_STATE);
       printf("onOffState==POWER_ON_FAIL pressCnt=%d,relase=%d, state=%d\r\n", gApplication.key.pressCnt, gApplication.key.releaseCnt, gApplication.key.state);
       DO_SLEEP();               //执行休眠
     }
     else if(onOffState == POWER_ON_ING)
     {
       power_module();
     }
     else
     {}
  }
  else
  {}
}

/**********************************************************************
 * @bref   正常处理阶段
 * @param  
 * @note   功能.
           1.档位控制
           2.电量显示
           3.关机检测
           4.充电检测
           5.休眠检测
**********************************************************************/
static void nomal_process(void)
{
  if(sample_task())
  {
    /* 电量显示 */
    power_module();
    
    /* 档位控制 */
    motor_control_module();
    
    /* 关机检测 */
    if(onoff_detect_module(1, 1500) == POWER_ON_SUCEESS)
    {
      printf("[nomal]shutdown, switch to ");
      //switch_runstate(POWERON_STATE);
      switch_runstate(POWEROFF_STATE);
      show_led_module(1, 1);      //关机前反向流水灯1次
      SET_MOTOR_CLOSE();
      DARK_RED_GREEN();
//      while(KEY_PRESS)IWDG_ReloadCounter();
//      DO_SLEEP();
    }
    
    /* 开机过程中插入USB充电 */
    if(gApplication.usbDect.state == 1) 
    {
      printf("[nomal]usb detected, switch to ");
      switch_runstate(CHARGE_STATE);
      LED12345_OFF;
      SET_MOTOR_CLOSE();
    }
    
    /* 低电检测 */
    uint8_t vbat_low = 0;
    if(get_motor_current() >= 1000)  //电流>1A, 电压<VBAT_SLEEP_VOLTAGE休眠
    {
      if(GET_VBAT_VOLTAGE() < VBAT_SLEEP_VOLTAGE)
      {
        vbat_low = 1;
      }
    }
    else if(GET_VBAT_VOLTAGE() < (VBAT_SLEEP_VOLTAGE+VOLTAGE_OFFSET)) //电流<1A,电压<(VBAT_SLEEP_VOLTAGE+VOLTAGE_OFFSET)休眠
    {
      vbat_low = 1;
    }
    
    if(vbat_low)
    {
      printf("[nomal]vbat too low, switch to ");
      switch_runstate(POWEROFF_STATE);
      SET_MOTOR_CLOSE();
      DARK_RED_GREEN();
    }
  }

}

/**********************************************************************
 * @bref   充电阶段
 * @param  
 * @note   功能
           1.充电开始时的流水灯
           2.电量显示
           3.充电检测（拔出）
**********************************************************************/
static void charging_process(void)
{
  /* 电量显示 */
  power_module();
  
  /* 退出USB充电 */
  if(gApplication.usbDect.state != 1) 
  {
    printf("[charge]usb disappear, switch to ");
    switch_runstate(POWERON_STATE);
    SET_MOTOR_CLOSE();
    DO_SLEEP();        
  }
}

/**********************************************************************
 * @bref   关机流程
 * @param  
 * @note   功能
**********************************************************************/
void poweroff_process(void)
{
  static uint8_t offstate = 0;
  static SYSTEM_TIME_T ttimer = {0};
  
  switch(offstate)
  {
    case 0:
      ttimer = get_systime();
      offstate = 1;
      break;
    
    case 1:
      if(compare_systime_ms(&ttimer) >= 200)       //200ms灯灭
      {
        offstate = 2;  
        CLOSE_HALL_POWER;
      }
      break;
    
    case 2:
      if(!KEY_PRESS) 
      {
        offstate = 0;
        switch_runstate(POWERON_STATE);
        DO_SLEEP();                 //执行休眠
      }//IWDG_ReloadCounter();		  //短按开机，等到抬起按键才休眠，防止按过头重新开机
      break;
    default:break;
  }
}


void print_app_info(void)
{
  printf("current=%d, vbus=%d, state=%d, charge=%d\r\n", 
         sControl.sample.uCurrentMA,
         (uint32_t)sControl.sample.uVBus,
         gApplication.runState,
         gApplication.charging); 
}
