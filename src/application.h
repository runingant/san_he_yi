#ifndef __application_h__
#define __application_h__
#include "sw_tools.h"
#include "square_wave_control.h"

/*=======================================================================
          参数定义
=========================================================================*/

/* 电压定义(单位mV) */
#define VBAT_VOLTAGE_RED            9000 //电量低(红灯)电压
#define VBAT_VOLTAGE_YELLOW         10500 //电量中等(黄灯)电压
#define VBAT_VOLTAGE_GREEN          12000 //电量高(绿灯)电压
#define VOLTAGE_OFFSET              500   //各档位判断电压回差
#define VBAT_SLEEP_VOLTAGE          8000  //休眠电压门限

/*=======================================================================
          接口定义
=========================================================================*/
#define GET_VBAT_VOLTAGE()         get_vbat_voltage()   //获取VBAT电压
#define GET_MOTOR_CURRENT()        get_motor_current()  //获取电机电流

/* 休眠 */
#define DO_SLEEP()                 sleep()   //执行休眠

/* 控制档位 */
/*
//MOTOR_MAX_SPEED == 2800
#define SET_MOTOR_LEVEL1_SPEED()   set_input_pwm_percent(0.6)	//1757	
#define SET_MOTOR_LEVEL2_SPEED()   set_input_pwm_percent(0.7)	//2057
#define SET_MOTOR_LEVEL3_SPEED()   set_input_pwm_percent(0.8)	//2357
#define SET_MOTOR_LEVEL4_SPEED()   set_input_pwm_percent(0.9)	//2674
#define SET_MOTOR_CLOSE()          set_input_pwm_percent(0)*/

//MOTOR_MAX_SPEED == 3200
#define SET_MOTOR_LEVEL1_SPEED()   set_input_pwm_percent(0.6)	//2000
#define SET_MOTOR_LEVEL2_SPEED()   set_input_pwm_percent(0.68)	//2300
#define SET_MOTOR_LEVEL3_SPEED()   set_input_pwm_percent(0.77)	//2600
#define SET_MOTOR_LEVEL4_SPEED()   set_input_pwm_percent(0.86)	//2900
#define SET_MOTOR_CLOSE()          set_input_pwm_percent(0)

/*=======================================================================
          声明函数
=========================================================================*/
void driver_task(void);
void application_init(void);
void application_task(void);

void print_app_info(void);
void vbate_charge_detect(void);
#endif
