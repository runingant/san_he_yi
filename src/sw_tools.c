#include "hk32f0xx.h"
#include "sw_tools.h" 

/*=======================================================================
                          【系统时钟】
=========================================================================*/
SYSTEM_TIME_T gSystemTime; //系统时钟

/**********************************************************************
 * @bref	 系统时钟累加
 * @param  
 * @note   中断调用
 **********************************************************************/
void update_systime(uint32_t nMs)
{
	gSystemTime.ms += nMs;
	while(gSystemTime.ms >= 1000)
	{
		gSystemTime.sec++;
		gSystemTime.ms -= 1000;
	}
}

/**********************************************************************
 * @bref	 获取系统时钟
 * @param  
 **********************************************************************/
SYSTEM_TIME_T get_systime(void)
{
	return gSystemTime;
}


/**********************************************************************
 * @bref	 获取系统时钟
 * @param  
 **********************************************************************/
uint32_t compare_systime_ms(SYSTEM_TIME_T *pTime)
{
	int32_t subTimeSec = 0;
	int32_t subTimeMs = 0;
	int32_t outMs = 0;

	subTimeSec = gSystemTime.sec - pTime->sec;
	subTimeMs = gSystemTime.ms - pTime->ms;

	outMs = subTimeSec*1000 + subTimeMs;
	if(outMs < 0)
	{
		outMs = 0;
	}
	return ((uint32_t) outMs);
}

/**********************************************************************
 * @bref	 延时一定时间
 * @param  不准
 **********************************************************************/
void delay_ms(uint16_t m)
{
  int i = 0;
  
  while(m--)
  {
    for(i=0;i<1000;i++);
  }
}

void delay_01ms(uint16_t m)
{
  int i = 0;
  
  while(m--)
  {
    for(i=0;i<100;i++);
  }
}

/*=======================================================================
|                             【时间测量】                                    |
=========================================================================*/

/**********************************************************************
 * @bref	通过TIMER2的CNT值来判定延时
 * @param  
 **********************************************************************/
extern inline void measure_start(MEASURE_T *pMeasure)
{
  pMeasure->start = TIM3->CNT;
}

extern inline void measure_stop(MEASURE_T *pMeasure)
{
  pMeasure->stop = TIM3->CNT; 
	pMeasure->nowCounter = pMeasure->stop - pMeasure->start;
	if(pMeasure->nowCounter > pMeasure->maxCounter)
	{
  	pMeasure->maxCounter = pMeasure->nowCounter;
    pMeasure->maxCounterStart = pMeasure->start;
    pMeasure->maxCounterStop = pMeasure->stop;
	}
}


/*=======================================================================
|                             【打印服务】                                    |
=========================================================================*/

int32_t s_debugTable[3][DEBUG_LOG_NUM] = {0}; 	//存储BUFFER
int 		s_debugNumber = 0;											//已存储数量
int 		s_debugIndex = 0; 											//当前存储位置
uint8_t s_debugLock = 0;												//lock标志

/**********************************************************************
 * @bref	 保存LOG
 * @param  a - 第1个参数
 *         b - 第2个参数
 *         c - 第3个参数
 *         d - 第4个参数
 **********************************************************************/
void debug_save_log(int32_t a, int32_t b, int32_t c, int32_t d)
{
	if((s_debugLock == 0) && (s_debugNumber < DEBUG_LOG_NUM))
	{
		s_debugTable[0][s_debugIndex] = a;
		s_debugTable[1][s_debugIndex] = b;
		s_debugTable[2][s_debugIndex] = c;
//		s_debugTable[3][s_debugIndex] = d;
		s_debugIndex++;
		s_debugNumber++;
	}
}

/**********************************************************************
 * @bref	 打印LOG
 * @param  
 **********************************************************************/
void debug_print_log(void)
{
	uint16_t i = 0;

	if(s_debugNumber >= DEBUG_LOG_NUM)
	{
		s_debugLock = 1;
		for(i=0; i<DEBUG_LOG_NUM; i++)	 
		{
//			printf("a=%d, b=%d, c=%d, d=%d\r\n", s_debugTable[0][i],
//			                                     s_debugTable[1][i],
//			                                     s_debugTable[2][i],
//			                                     s_debugTable[3][i]/*g_hall.measuredSpeed*/);  
      printf("a=%d, b=%d, c=%d\r\n", s_debugTable[0][i], s_debugTable[1][i], s_debugTable[2][i]);
		}
		s_debugNumber = 0;
		s_debugIndex = 0;
    printf("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
    //delay_ms(100); 
    s_debugLock = 0;  
	}
}



