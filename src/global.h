/************** Copyright (C), 2016-2026,XXXXXXXXXXXXXXXXXXXXXXXXX *************

********************************************************************************/
#ifndef __GLOBAL_H__
#define	__GLOBAL_H__

#include "stdio.h"
#include <stdint.h> //包含uint8_t等类型
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>


#define dis_irq()		__disable_irq()
#define en_irq()		__enable_irq()

/* 调试模式，启用该宏则可以进入调试模式 */
#define DEBUG_MODE 

/* printf调试宏开关 */
#define __DEBUG__    
#ifdef __DEBUG__
/* 告警打印 */  
#define aprintf(format,...) printf("File: "__FILE__", Line: %05d: "format"\n", __LINE__, ##__VA_ARGS__)
/* 调试打印 */
#define dprintf(format,...) printf(format, ##__VA_ARGS__)  
#else
#define aprintf(format,...) 
#define dprintf(format,...)  
#endif

/* 状态量定义 */
enum
{
	FALSE = 0,
	TRUE,
	OFF = 0,
	ON,
	CLOSE = 0,
	OPEN,
	LOW = 0,
	HIGH,
	NOFULL = 0,
	FULL
};

#define BIT0    (1<<0)
#define BIT1    (1<<1)
#define BIT2    (1<<2)
#define BIT3    (1<<3)
#define BIT4    (1<<4)
#define BIT5    (1<<5)
#define BIT6    (1<<6)
#define BIT7    (1<<7)

#define UNBIT0    (0<<0)
#define UNBIT1    (0<<1)
#define UNBIT2    (0<<2)
#define UNBIT3    (0<<3)
#define UNBIT4    (0<<4)
#define UNBIT5    (0<<5)
#define UNBIT6    (0<<6)
#define UNBIT7    (0<<7)

typedef void (*pTaskHook)(void);    
typedef void (*pvfunc)(void);
typedef uint8_t (*pucfunc)(void);

#ifndef bool
    #define bool    unsigned char
    #define true    (~false)
    #define false   0
#endif

typedef void (*pfuc)(void);   

extern void print_now_time(void); //test
#endif /* __GLOBAL_H__ */

