#ifndef __debug_h__
#define __debug_h__

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

/*=======================================================================
          打印debug参数定义
=========================================================================*/
#define DEBUG_LOG_NUM          (50)   //打印数量

#define MARK_NULL              (0)   //不打印任何东西
#define RECORD_SW_PID          (1)   //打印PID记录的数据
#define RECORD_SPEED_CALC      (2)   //打印速度计算的数据
#define RECORD_HALL_IRQ        (3)   //中断的数据
#define RECORD_HALL            (4)   //打印HALL信息
#define RECORD_CURRENT         (5)   //电流信息
#define RECORD_PRINT_NOW       (100) //实时打印
#define MARK                   MARK_NULL


//#define dbg(format, args...)   printf("[%s/%s](%d)" format, __FILE__,__FUNCTION__,__LINE__, ##args)

//#define dbg2(string...)\
//													do{\
//														if(get_record_log_level() != 0)\
//														{\
//															printf("%s(%d)[%s]: ", __FILE__, __LINE__, __FUNCTION__);\
//															printf(string);\
//															printf("\n");\
//														}\
//													}while(0)


void debug_save_log(int32_t a, int32_t b, int32_t c, int32_t d);
void debug_print_log(void);
//void debug_save_one(int32_t val);


void delay_02ms(uint16_t m);
void delay_ms(uint16_t m);

#endif
