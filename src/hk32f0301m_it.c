/**
  ******************************************************************************
  * @file    hk32f0301m_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "hk32f0301m.h"
#include "hk32f0301m_it.h"
#include "square_wave_control.h"
#include "square_wave_sample.h" 
#include "sw_tools.h"
#include "platform_service.h"

/* 宏定义/结构定义 ----------------------------------------------------------*/

/* 变量定义 ----------------------------------------------------------*/

extern uint8_t g_sampleChannelNo;         //采样通道
extern uint8_t g_currentSampleSuccess;    //电流采样成功标志

/**********************************************************************
 * @bref	 系统滴答定时器 1ms中断
 * @param  
 **********************************************************************/
void SysTick_Handler(void)
{
  static uint32_t runtimes = 0;
	SW_CONTROL_S *pControl = &sControl;
 
	/* 刷新PWM输出 */
	TIM_GenerateEvent(TIM1, TIM_EventSource_COM);   //产生COM事件,更新PWM

	/* 控制周期 */
	if(runtimes++ >= 10) //选用10ms控制周期
	{
		if(check_output_enable(pControl))
		{
			auto_mode_output(pControl);
		}
		runtimes = 0;
	}
	
	sw_calculate_speed(&sControl);

	/* 判断速度 */
  if(pControl->speedValidCounter)
  {
    pControl->speedValidCounter--;
  }
	
	/* 任务指针 */
	platform_runHook();

	/* 系统时间累加 */
  update_systime(1);
}


/**********************************************************************
 * @bref	TIM2中断，主要用于HALL
 * @param  
 **********************************************************************/
void TIM2_IRQHandler(void)
{
  /*判断是否是UPDATE事件*/
	if(TIM_GetFlagStatus(TIM2,TIM_FLAG_Update))
	{
		TIM_ClearFlag(TIM2,TIM_FLAG_Update);
	}
  
  
  /*判断是否是CC1事件*/
	if(TIM_GetFlagStatus(TIM2,TIM_FLAG_CC1))
	{
		TIM_ClearFlag(TIM2,TIM_FLAG_CC1);
    sw_hall_switch(&sControl, 0);  
	}
}


  
/**********************************************************************
 * @bref	ADC1中断,负责采样完成后的处理
 * @param  
 **********************************************************************/
void ADC1_IRQHandler(void)
{
  uint32_t ret = 0;
  
	/* 转换完成中断标志 */
	if (ADC_GetITStatus(ADC1, ADC_IT_EOC) != RESET)
	{
		ADC_ClearITPendingBit(ADC1, ADC_IT_EOC);
		switch(g_sampleChannelNo)
		{
			/* 输入PWM占空比 */
			case 0: 
        if(sControl.controlMode == MODE_AUTO)
        {
          ret = sample_filter(SAMPLE_INPUT_PWM, ADC_GetConversionValue(ADC1), NO_FILTER, 2);   //闭环模式下，PWM滤波次数2
        }
        else if(sControl.controlMode == MODE_PWM)
        {
          ret = sample_filter(SAMPLE_INPUT_PWM, ADC_GetConversionValue(ADC1), NO_FILTER, 0);   //开环模式下，输入PWM不滤波
        }
        sControl.sample.fInputPwmPercent = (float)ret/(float)4096; //计算百分比
        //sControl.sample.fInputPwmPercent = 0.6;//TEST
				g_sampleChannelNo = 1;
				g_currentSampleSuccess = 1;
				break;

			/* VBUS采样 */
			case 1:
				sample_filter(SAMPLE_VBS, ADC_GetConversionValue(ADC1), AVERAGE_FILTER, FIFO_NUMBER);  
				g_sampleChannelNo = 2;
				break;

			/* 电流 */
			case 2:
				ret = sample_filter(SAMPLE_CURRENT, ADC_GetConversionValue(ADC1), AVERAGE_FILTER, 10);  
        sControl.sample.uCurrentMA = ret*1000 / 24;  //计算电流mA
				g_sampleChannelNo = 0;
				break;
				
			default:break;
		}
		ADC_StopOfConversion(ADC1);  //任何一次采样完成后，都关闭ADC
	}
}



/**********************************************************************
 * @bref	 串口1中断
 * @param  
 **********************************************************************/
void USART1_IRQHandler(void)
{
  USART_ClearFlag(USART1, USART_IT_RXNE);
  USART_ClearFlag(USART1, USART_IT_TC); 
}


/**********************************************************************
 * @bref	TIM1 COM中断 
 * @param  
 * @note  NOT USED
 **********************************************************************/
void TIM1_UP_TRG_COM_IRQHandler(void)
{
  TIM_ClearITPendingBit(TIM1, TIM_IT_COM);   //清除COM事件
}


/**********************************************************************
 * @bref	0.5ms中断
 * @param  
 **********************************************************************/
void TIM6_IRQHandler(void)
{
  if (TIM_GetITStatus(TIM6, TIM_IT_Update) != RESET)
  {
    TIM_ClearITPendingBit(TIM6, TIM_IT_Update);
  }
}




/******************************************************************************/
/*           Cortex-M0 Processor Interruption and Exception Handlers          */ 
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{

}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  while (1)
  {
  }
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{
}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{
}


/******************************************************************************/
/* hk32f0301m Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_hk32f0301m.s).                    */
/******************************************************************************/
/************************ (C) COPYRIGHT HKMicroChip *****END OF FILE****/
