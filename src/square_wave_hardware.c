#include "square_wave_hardware.h"

/*=======================================================================
          宏定义
=========================================================================*/

/*=======================================================================
          参数定义
=========================================================================*/
//DMA_HandleTypeDef hdma_adc1;
USART_InitTypeDef USART_InitStructure;
TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure; 
TIM_OCInitTypeDef  TIM_OCInitStructure;
TIM_ICInitTypeDef	 TIM_ICInitStructure;
TIM_BDTRInitTypeDef TIM_BDTRInitStructure;
EXTI_InitTypeDef EXTI_InitStructure;
//COMP_InitTypeDef COMP_InitStructure;
//NVIC_InitTypeDef    NVIC_InitStructure;
USART_InitTypeDef USART_InitStructure;

/*=======================================================================
         基本外设初始化
=========================================================================*/

/**********************************************************************
 * @bref	 工具函数，通用GPIO配置函数
 * @param  
 **********************************************************************/
static void gpio_config(void* port, uint32_t pin, GPIOMode_TypeDef mode, GPIOPuPd_TypeDef pull)
{  
  GPIO_InitTypeDef GPIO_InitStructure = {0};
  GPIO_InitStructure.GPIO_Pin = pin ;
  GPIO_InitStructure.GPIO_Mode = mode;
  //if((mode == GPIO_Mode_OUT) || (mode == GPIO_Mode_AF))
  if(mode != GPIO_Mode_AN)
	{
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;  //输出默认都是PP
	}
	GPIO_InitStructure.GPIO_PuPd = pull ;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_3; //10M
  GPIO_Init(port, &GPIO_InitStructure); 
}

/**********************************************************************
 * @bref	 初始化CPU、时钟
 * @param  
 **********************************************************************/
void drv_init_cpu(void)
{
	/* Setup SysTick Timer for 1 msec interrupts  */
	if (SysTick_Config((SystemCoreClock) / 1000))
	{ 
		while (1);
	}
//	NVIC_SetPriority(SysTick_IRQn, 0x0);

	/* Open Clocks */
  RCC_APB1PeriphClockCmd( RCC_APB1Periph_PWR, ENABLE);      //需要休眠必须打开这个时钟，否则PWR_EnterSTANDBYMode()休眠后电流还会有0.1mA
	RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOA, ENABLE );
	RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOB, ENABLE );
	RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOC, ENABLE );
	RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOD, ENABLE );
  RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOF, ENABLE );
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_SYSCFG, ENABLE);
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_TIM1 , ENABLE);   //打开TIMER时钟
  RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM3 , ENABLE);
  RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM6 , ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);      //打开ADC时钟 
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
}


/**********************************************************************
 * @bref	 初始化ADC1
 * @param  
 **********************************************************************/
void drv_init_adc1(void)
{
  /* 航顺DEMO */
  GPIO_InitTypeDef    GPIO_InitStructure;
	ADC_InitTypeDef     ADC_InitStructure;
	
	/* GPIOC, GPIOD Periph clock enable */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  
  /* ADC1 Periph clock enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	
	/* Configure ADC Ch2(PC4), Ch3(PD3), Ch4(PD2) as analog input */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Pin = CURRENT_PIN;
	GPIO_Init(CURRENT_PORT, &GPIO_InitStructure);
  
  GPIO_InitStructure.GPIO_Pin = VBUS_PIN;
  GPIO_Init(VBUS_PORT, &GPIO_InitStructure);
	
	/* ADCs DeInit */  
  ADC_DeInit(ADC1);

	/* Configure the ADC in continuous mode with a resolution equal to 12 bits  */
  ADC_InitStructure.ADC_ContinuousConvMode = DISABLE; 
  ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConvEdge_None;
  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  ADC_InitStructure.ADC_ScanDirection = ADC_ScanDirection_Upward;
  ADC_Init(ADC1, &ADC_InitStructure);
	
	ADC_DiscModeCmd(ADC1, ENABLE);
	
	/* Configure ADC clock source: 16MHz */
	RCC_ADCCLKConfig(RCC_ADCCLK_PCLK_Div2);//RCC_ADCCLKConfig(RCC_ADCCLK_HSI48M_Div3);
	
	/* Convert the ADC chx with 55.5 Cycles as sampling time */ 
  ADC_ChannelConfig(ADC1, CURRENT_ADC1_CHANNEL, ADC_SampleTime_239_5Cycles);
  ADC_ChannelConfig(ADC1, VBUS_ADC1_CHANNEL, ADC_SampleTime_55_5Cycles);
	
	/* ADC Calibration */
  ADC_GetCalibrationFactor(ADC1);
	
  /* Configures ADC interrupt: Enable EOC and EOSE interrupt */
	//ADC_ITConfig(ADC1, ADC_IT_EOC | ADC_IT_EOSEQ, ENABLE);
	ADC_ITConfig(ADC1, ADC_IT_EOC, ENABLE);
	
	/* Set ADC Interrupt Priority */
	NVIC_SetPriority(ADC1_IRQn, 3);
	
	/* Enable ADC interrupt */
	NVIC_EnableIRQ(ADC1_IRQn);
	
	/* Enable the ADC peripheral */
  ADC_Cmd(ADC1, ENABLE);     
  
  /* Wait the ADRDY flag */
  while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_ADRDY));
	
  /* ADC regular Software Start Conv */ 
  //ADC_StartOfConversion(ADC); 
}


/*=======================================================================
        配置HALL相关
=========================================================================*/

/**********************************************************************
 * @bref	 TIM2初始化，用于触发HALL中断
 * @param
 * @note   异或三个hall信号，任何一个发生变化则产生中断
  030 GPIO_AF_1
  0301M GPIO_AF_4
 **********************************************************************/
static __inline void drv_init_hall_with_TIM2(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;

	/* 配置GPIO */
	ENABLE_ADC_GPIO_CLK();	 
	gpio_config(HALL_1_PORT, HALL_1_PIN, GPIO_Mode_AF, GPIO_PuPd_DOWN);  //配置为IRQ
	gpio_config(HALL_2_PORT, HALL_2_PIN, GPIO_Mode_AF, GPIO_PuPd_DOWN);  
	gpio_config(HALL_3_PORT, HALL_3_PIN, GPIO_Mode_AF, GPIO_PuPd_DOWN);
	GPIO_PinAFConfig(HALL_1_PORT,  HALL_1_PIN_SOURCE,  GPIO_AF_1);	 //AF1,配置为TIM3_CH1
	GPIO_PinAFConfig(HALL_2_PORT,  HALL_2_PIN_SOURCE,  GPIO_AF_1);	 //AF1,配置为TIM3_CH2
	GPIO_PinAFConfig(HALL_3_PORT,  HALL_3_PIN_SOURCE,  GPIO_AF_1);	 //AF1,配置为TIM3_CH3

	/* TIM2基础配置 */
  TIM_TimeBaseStructure.TIM_Prescaler = 8;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period = 65535;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
  TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

	/* 配置HALL测量 */
	TIM_SelectHallSensor(TIM3,ENABLE); 
	TIM_ICInitStructure.TIM_ICPolarity =	TIM_ICPolarity_Rising;
	TIM_ICInitStructure.TIM_ICPrescaler =	TIM_ICPSC_DIV1;
	TIM_ICInitStructure.TIM_ICFilter =	16;   //提高ICFilter能够缓解丢失HALL中断问题
	TIM_ICInitStructure.TIM_ICSelection =	TIM_ICSelection_TRC;
	TIM_ICInitStructure.TIM_Channel = TIM_Channel_1;
	TIM_ICInit(TIM3,&TIM_ICInitStructure);

	/* 测量PWM输入 */
//	TIM_ICInitStructure.TIM_Channel = TIM_Channel_4;
//  TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;
//  TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
//  TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
//  TIM_ICInitStructure.TIM_ICFilter = 0x00;
//  TIM_PWMIConfig(TIM2, &TIM_ICInitStructure);

//	TIM_SelectOutputTrigger(TIM2,TIM_TRGOSource_OC2Ref);
//	TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);

	/* 开中断使能*/
	TIM_ITConfig(TIM3,TIM_IT_CC1,ENABLE);
//	TIM_SelectInputTrigger(TIM2, TIM_TS_TI1FP1);

//	TIM_UpdateRequestConfig(TIM2,TIM_UpdateSource_Regular);//更新请求选择上溢、下溢
	TIM_SelectInputTrigger(TIM3,TIM_TS_TI1F_ED);//选择触发边沿检测模式
	TIM_SelectSlaveMode(TIM3,TIM_SlaveMode_Reset);//从模式选择复位模式
	TIM_Cmd(TIM3,ENABLE); //ENABLE

	/* 打开NVIC中断 */
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_Init(&NVIC_InitStructure); //正常模式下才打开HALL中断
	
}


/* 初始化测量timer
   测量timer只是提供一个计数器而已，计数器到达最大值之后不清零，由程序清零
*/
void drv_init_measureTimer(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct = {0};

	/* TIM6基本配置 */
	TIM_InternalClockConfig(MEASURE_TIMER); //配置时钟源
	TIM_TimeBaseStructInit(&TIM_TimeBaseInitStruct);
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;     //不分频
	TIM_TimeBaseInitStruct.TIM_Period = 0xFFFF;    
	TIM_TimeBaseInitStruct.TIM_Prescaler = MEASURE_TIMER_PRESCALER;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up; //向上计数
	TIM_TimeBaseInit(MEASURE_TIMER, &TIM_TimeBaseInitStruct);

	TIM_SelectOnePulseMode(MEASURE_TIMER, TIM_OPMode_Single);    //设置为单脉冲模式

	/* 开中断源 */
	TIM_ITConfig(MEASURE_TIMER, TIM_IT_Update, ENABLE);
	
	/* 开NVIC中断 */
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStructure.NVIC_IRQChannel = TIM6_IRQn;
	NVIC_Init(&NVIC_InitStructure); //正常模式下才打开HALL中断
}

void drv_start_measureTimer(void)
{
	TIM_Cmd(MEASURE_TIMER, ENABLE);           //使能计数器
}

uint32_t drv_get_measureTimer(void)
{
	uint32_t ret = 0;

	/* 获取cnter */
	ret = MEASURE_TIMER->CNT;
	if((ret == 0) && (MEASURE_TIMER->SR & 0x0001)) //溢出
	{
		ret = 0xFFFF;
		MEASURE_TIMER->SR = 0;
		TIM_Cmd(MEASURE_TIMER, ENABLE);           //使能计数器
	}
	if((MEASURE_TIMER->CR1 & 0x0001) == 0)  
	{
		TIM_Cmd(MEASURE_TIMER, ENABLE);
	}

	/* 重新开始下一次 */
	MEASURE_TIMER->CNT = 0;
	return ret;
}



/**********************************************************************
 * @bref	 初始化HALL，需要用到TIM3
 * @param  
**********************************************************************/
void drv_init_hall(void)
{
	drv_init_hall_with_TIM2();
	drv_init_measureTimer();
	drv_start_measureTimer();
}

/**********************************************************************
 * @bref	 初始化方向检测
 * @param  
**********************************************************************/
void drv_init_direction(void)
{
  gpio_config(MOTOR_DIR_PORT, MOTOR_DIR_PIN, GPIO_Mode_IN, GPIO_PuPd_NOPULL);  //配置为输入
//  gpio_config(MOTOR_DIR_PORT, MOTOR_DIR_PIN, GPIO_Mode_OUT, GPIO_PuPd_NOPULL);  //配置为输入
//  GPIO_WriteBit(MOTOR_DIR_PORT, MOTOR_DIR_PIN, Bit_SET);
}


/**********************************************************************
 * @bref   毅瑞德电机霍尔
 * @param  
 * @note   orignal sequence: HALL1<<2, HALL2<<1, HALL1

3取反
132  X
123  XXX
312  xxx
321  xxx
213  X
231  xxx

2取反
123
**********************************************************************/
static uint8_t get_yiruide_motor(void)
{
	uint8_t hall;
#if(BOARD == BOARD_YI_RUI_DE)
	hall  =	 GPIO_ReadInputDataBit( HALL_2_PORT, HALL_2_PIN ) << 2
						 | (GPIO_ReadInputDataBit( HALL_3_PORT, HALL_3_PIN ) == 0) << 1
						 | GPIO_ReadInputDataBit( HALL_1_PORT, HALL_1_PIN );    //毅瑞德电机PC6需要反向
#elif(BOARD == BOARD_SQUARE_WAVE_105)
	hall  =	 GPIO_ReadInputDataBit( HALL_1_PORT, HALL_1_PIN ) << 2
						 | GPIO_ReadInputDataBit( HALL_2_PORT, HALL_2_PIN ) << 1
						 | GPIO_ReadInputDataBit( HALL_3_PORT, HALL_3_PIN ); 
#elif(BOARD == BOARD_GUO_MENG)
	hall	=   GPIO_ReadInputDataBit( HALL_1_PORT, HALL_1_PIN )<< 2
					 | (GPIO_ReadInputDataBit( HALL_2_PORT, HALL_2_PIN )==0) << 1
					 | GPIO_ReadInputDataBit( HALL_3_PORT, HALL_3_PIN );  //HALL2需要反向（U5）

#endif
	return hall;
}

static uint8_t get_yiruide_motor2(void)
{
	uint8_t hall;
	hall  =	 GPIO_ReadInputDataBit( HALL_3_PORT, HALL_3_PIN ) << 2
						 | GPIO_ReadInputDataBit( HALL_2_PORT, HALL_2_PIN ) << 1
						 | GPIO_ReadInputDataBit( HALL_1_PORT, HALL_1_PIN ); 
	return hall;
}


/**********************************************************************
 * @bref   38电机
 * @param  
 * @note   
**********************************************************************/
#define HALL38x1   GPIO_ReadInputDataBit( HALL_1_PORT, HALL_1_PIN )
#define HALL38x2   GPIO_ReadInputDataBit( HALL_2_PORT, HALL_2_PIN ) 
#define HALL38x3   GPIO_ReadInputDataBit( HALL_3_PORT, HALL_3_PIN ) 
static uint8_t get_38comm_motor(void)
{
	uint8_t hall = 0;	
	hall  =	 (HALL38x1 << 2)|(HALL38x3 << 1)| HALL38x2;		//五台38样机  
//	hall  =	 (HALL38x1 << 2)|(HALL38x2 << 1)| HALL38x3;		//foc那台38样机
//	hall  =	 (HALL38x3 << 2)|(HALL38x2 << 1)| HALL38x1;		//11月12无感拆装电机
  
//  hall  =	 (HALL38x2 << 2)|(HALL38x1 << 1)| HALL38x3;   
//	hall  =	 (HALL38x2 << 2)|(HALL38x3 << 1)| HALL38x1; 
//	hall  =	 (HALL38x1 << 2)|(HALL38x2 << 1)| HALL38x3;   
//	hall  =	 (HALL38x1 << 2)|(HALL38x3 << 1)| HALL38x2;  
//	hall  =	 (HALL38x3 << 2)|(HALL38x1 << 1)| HALL38x2; 
//	hall  =	 (HALL38x3 << 2)|(HALL38x2 << 1)| HALL38x1;   
	return hall;
}

/**********************************************************************
 * @bref   国梦电机
 * @param  
 * @note   orignal sequence: HALL1<<2, HALL2<<1, HALL1
**********************************************************************/
#define HALL1  (GPIO_ReadInputDataBit( HALL_1_PORT, HALL_1_PIN ) == 0)
#define HALL3  (GPIO_ReadInputDataBit( HALL_3_PORT, HALL_3_PIN ) == 0)
#define HALL2  GPIO_ReadInputDataBit( HALL_2_PORT, HALL_2_PIN )
static uint8_t get_guomeng_motor(void)
{
	uint8_t hall;
	hall = HALL3 << 2
      |  HALL1 << 1
      |  HALL2; 
	return hall;
}

/**********************************************************************
 * @bref   公模12槽电机
 * @param  
 * @note   orignal sequence: HALL1<<2, HALL2<<1, HALL1

1和3取反        2取反     1取反    3取反
 213  √√       ××           ××      √ 
 231  ××       √√√          ××      ××
 123  ××       ××           ××      ××
 132  ××       ××           ××      ××
 312  ××       √            ××      ××
 321  √        ××           ××      ××

**********************************************************************/
/*
#define HALL11  (GPIO_ReadInputDataBit( HALL_1_PORT, HALL_1_PIN )==0)
#define HALL22  (GPIO_ReadInputDataBit( HALL_3_PORT, HALL_3_PIN )==0)
#define HALL33  GPIO_ReadInputDataBit( HALL_2_PORT, HALL_2_PIN )
*/

#define HALL11  GPIO_ReadInputDataBit( HALL_1_PORT, HALL_1_PIN )
#define HALL22  (GPIO_ReadInputDataBit( HALL_2_PORT, HALL_2_PIN )==0)
#define HALL33  GPIO_ReadInputDataBit( HALL_3_PORT, HALL_3_PIN )

static uint8_t get_comm_12groove_motor(void)
{
	uint8_t hall;
	#if(BOARD == BOARD_GUO_MENG)
//	hall  =	 (HALL22 << 2)|(HALL11 << 1)| HALL33; 
	hall  =	 (HALL22 << 2)|(HALL33 << 1)| HALL11; 
//	hall  =	 (HALL11 << 2)|(HALL22 << 1)| HALL33; 
//	hall  =	 (HALL11 << 2)|(HALL22 << 1)| HALL33; 
//	hall  =	 (HALL33 << 2)|(HALL11 << 1)| HALL22; 
//	hall  =	 (HALL33 << 2)|(HALL22 << 1)| HALL11; 
	#else
	hall  =	 GPIO_ReadInputDataBit( HALL_1_PORT, HALL_1_PIN ) << 2
							 | GPIO_ReadInputDataBit( HALL_2_PORT, HALL_2_PIN ) << 1
							 | GPIO_ReadInputDataBit( HALL_3_PORT, HALL_3_PIN ); 
	#endif
	return hall;
}

/**********************************************************************
 * @bref   公模9槽电机
 * @param  
 * @note   orignal sequence: HALL1<<2, HALL2<<1, HALL1
**********************************************************************/
#define HALL9x1   GPIO_ReadInputDataBit( HALL_1_PORT, HALL_1_PIN )
#define HALL9x2   GPIO_ReadInputDataBit( HALL_2_PORT, HALL_2_PIN ) 
#define HALL9x3   GPIO_ReadInputDataBit( HALL_3_PORT, HALL_3_PIN ) 
static uint8_t get_comm_9groove_motor(void)
{
  uint8_t hall;
#if(BOARD == BOARD_SAN_HE_YI)
//  hall  =	 (HALL9x1 << 2)|(HALL9x2 << 1)| HALL9x3;   //x可以转，电流很大
//  hall  =	 (HALL9x1 << 2)|(HALL9x3 << 1)| HALL9x2;   //x
//  hall  =	 (HALL9x2 << 2)|(HALL9x1 << 1)| HALL9x3;   //x不能转，抖动
  hall  =	 (HALL9x2 << 2)|(HALL9x3 << 1)| HALL9x1;   //x可以转，电流很大
//  hall  =	 (HALL9x3 << 2)|(HALL9x1 << 1)| HALL9x2;   //可以转，电流很大
//  hall  =	 (HALL9x3 << 2)|(HALL9x2 << 1)| HALL9x1;  //不能转
#else
  hall  =	 (HALL9x1 << 2)|(HALL9x2 << 1)| HALL9x3; 
#endif
	return hall;
}

/**********************************************************************
 * @bref	 获取HALL信号状态值
 * @param  
 * @note   orignal sequence: HALL1<<2, HALL2<<1, HALL1
**********************************************************************/
uint8_t drv_get_hallstate(void)
{
	uint8_t hall;
	
#if(MOTOR == MOTOR_COMM_12GROOVE)
	hall = get_comm_12groove_motor();
#elif(MOTOR == MOTOR_COMM_9GROOVE)
	hall = get_comm_9groove_motor();
#elif(MOTOR == MOTOR_GUO_MENG)
	hall = get_guomeng_motor();
#elif(MOTOR == MOTOR_YI_RUI_DE)
	hall = get_yiruide_motor();
#elif(MOTOR == MOTOR_YI_RUI_DE2)
	hall = get_yiruide_motor2();
#elif(MOTOR == MOTOR_38COMM_12GROOVE)
  hall = get_38comm_motor();
#endif
	return hall;
}


/*=======================================================================
        配置PWM相关
=========================================================================*/

/**********************************************************************
 * @bref   init pwm gpio 
 * @param  
 * @note   there's four modes,
           CONTROL_BY_GPIO -- N mos and P mos are both GPIO mode
           CONTROL_BY_PWM  -- N mos PWM, P mos PWM
           CONTROL_BY_PWM_GPIO -- N mos GPIO, P mos PWM    
           CONTROL_BY_GPIO_PWM -- P mos GPIO, N mos PWM    [default]
**********************************************************************/
static __inline void drv_init_pwm_gpio(void)
{
  ENABLE_PWM_PORT_CLK();   
  RCC_APB2PeriphClockCmd( RCC_APB2Periph_TIM1, ENABLE );

#if(CONTROL_TYPE==CONTROL_BY_GPIO)
  /* P管GPIO */
  CLOSE_U_H; CLOSE_V_H; CLOSE_W_H;
  gpio_config(PWM_CH1_PORT,  PWM_CH1_PIN,  GPIO_Mode_OUT, GPIO_PuPd_UP);
  gpio_config(PWM_CH2_PORT,  PWM_CH2_PIN,  GPIO_Mode_OUT, GPIO_PuPd_UP);
  gpio_config(PWM_CH3_PORT,  PWM_CH3_PIN,  GPIO_Mode_OUT, GPIO_PuPd_UP);
	/* N管GPIO */
  CLOSE_U_L; CLOSE_V_L; CLOSE_W_L;
  gpio_config(PWM_CH1N_PORT, PWM_CH1N_PIN, GPIO_Mode_OUT, GPIO_PuPd_UP);
  gpio_config(PWM_CH2N_PORT, PWM_CH2N_PIN, GPIO_Mode_OUT, GPIO_PuPd_UP);
  gpio_config(PWM_CH3N_PORT, PWM_CH3N_PIN, GPIO_Mode_OUT, GPIO_PuPd_UP);
	
#elif(CONTROL_TYPE==CONTROL_BY_PWM_GPIO)  
	/* P管PWM */
	gpio_config(PWM_CH1_PORT, PWM_CH1_PIN, GPIO_Mode_AF, GPIO_PuPd_UP);  //AF
	gpio_config(PWM_CH2_PORT, PWM_CH2_PIN, GPIO_Mode_AF, GPIO_PuPd_UP);
	gpio_config(PWM_CH3_PORT, PWM_CH3_PIN, GPIO_Mode_AF, GPIO_PuPd_UP);
	GPIO_PinAFConfig(PWM_CH1_PORT, PWM_CH1_PIN_SOURCE, GPIO_AF_2);   //030芯片是AF_2，0301M是AF_3
	GPIO_PinAFConfig(PWM_CH2_PORT, PWM_CH2_PIN_SOURCE, GPIO_AF_2);
	GPIO_PinAFConfig(PWM_CH3_PORT, PWM_CH3_PIN_SOURCE, GPIO_AF_2);

	/* N管GPIO */
	CLOSE_U_L; CLOSE_V_L; CLOSE_W_L;
  gpio_config(PWM_CH1N_PORT, PWM_CH1N_PIN, GPIO_Mode_OUT, GPIO_PuPd_UP);
  gpio_config(PWM_CH2N_PORT, PWM_CH2N_PIN, GPIO_Mode_OUT, GPIO_PuPd_UP);
  gpio_config(PWM_CH3N_PORT, PWM_CH3N_PIN, GPIO_Mode_OUT, GPIO_PuPd_UP);
	
#elif(CONTROL_TYPE == CONTROL_BY_GPIO_PWM)
	/* P管GPIO */
  CLOSE_U_H; CLOSE_V_H; CLOSE_W_H;
  gpio_config(PWM_CH1_PORT, PWM_CH1_PIN, GPIO_Mode_OUT, GPIO_PuPd_UP);
  gpio_config(PWM_CH2_PORT, PWM_CH2_PIN, GPIO_Mode_OUT, GPIO_PuPd_UP);
  gpio_config(PWM_CH3_PORT, PWM_CH3_PIN, GPIO_Mode_OUT, GPIO_PuPd_UP);

	/* N管PWM */
	gpio_config(PWM_CH1N_PORT, PWM_CH1N_PIN, GPIO_Mode_AF, GPIO_PuPd_UP); 
  gpio_config(PWM_CH2N_PORT, PWM_CH2N_PIN, GPIO_Mode_AF, GPIO_PuPd_UP);
  gpio_config(PWM_CH3N_PORT, PWM_CH3N_PIN, GPIO_Mode_AF, GPIO_PuPd_UP);
  GPIO_PinAFConfig(PWM_CH1N_PORT, PWM_CH1N_PIN_SOURCE, GPIO_AF_3);   //AF
  GPIO_PinAFConfig(PWM_CH2N_PORT, PWM_CH2N_PIN_SOURCE, GPIO_AF_3);
  GPIO_PinAFConfig(PWM_CH3N_PORT, PWM_CH3N_PIN_SOURCE, GPIO_AF_3);
#endif
}

/***********************************************************************
 * @bref   初始化PWM
 * @param  
 * -------------------------------------------------------------------------------
  The following Table  describes the TIM1 Channels states:
              -----------------------------------------------
             | Step1 | Step2 | Step3 | Step4 | Step5 | Step6 |
   ----------------------------------------------------------
  |Channel1  |   1   |   0   |   0   |   0   |   0   |   1   |
   ----------------------------------------------------------
  |Channel1N |   0   |   0   |   1   |   1   |   0   |   0   |
   ----------------------------------------------------------
  |Channel2  |   0   |   0   |   0   |   1   |   1   |   0   |
   ----------------------------------------------------------
  |Channel2N |   1   |   1   |   0   |   0   |   0   |   0   |
   ----------------------------------------------------------
  |Channel3  |   0   |   1   |   1   |   0   |   0   |   0   |
   ----------------------------------------------------------
  |Channel3N |   0   |   0   |   0   |   0   |   1   |   1   |
   ----------------------------------------------------------
 * -------------------------------------------------------------------------------
**********************************************************************/
void drv_init_tim1(void)
{
#if((CONTROL_TYPE == CONTROL_BY_GPIO_PWM) || (CONTROL_TYPE == CONTROL_BY_PWM_GPIO))
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure; 
  TIM_OCInitTypeDef  TIM_OCInitStructure;         //通道输出配置
	TIM_BDTRInitTypeDef TIM_BDTRInitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* TIMER基础初始化 */
  TIM_TimeBaseStructure.TIM_Prescaler         = 0;
  TIM_TimeBaseStructure.TIM_CounterMode       = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period            = PWMTIM_PERIOD_CYCLE;
  TIM_TimeBaseStructure.TIM_ClockDivision     = 0;
  TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
  TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);

	/* TIMER1的CH1-CH3配置为PWM输出模式 */
  TIM_OCInitStructure.TIM_OCMode        = TIM_OCMode_PWM1;//TIM_OCMode_Timing;
	TIM_OCInitStructure.TIM_OutputState   = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OutputNState  = TIM_OutputNState_Enable;
#if (BIAO_DING == 1)
	TIM_OCInitStructure.TIM_Pulse         = PWMTIM_PERIOD_CYCLE/3;   //标定状态下，PWM默认有输出
#else
	TIM_OCInitStructure.TIM_Pulse         = 0;
#endif
	TIM_OCInitStructure.TIM_OCPolarity    = TIM_OCPolarity_High;   //P管高导通
	TIM_OCInitStructure.TIM_OCNPolarity   = NMOS_DEFAULT_VALUE;    //105版本就无所谓了
	TIM_OCInitStructure.TIM_OCIdleState   = TIM_OCIdleState_Reset;
	TIM_OCInitStructure.TIM_OCNIdleState  = TIM_OCNIdleState_Reset;
  TIM_OC1Init(TIM1, &TIM_OCInitStructure);            //配置通道1\2\3
  TIM_OC2Init(TIM1, &TIM_OCInitStructure);
  TIM_OC3Init(TIM1, &TIM_OCInitStructure);

	/* 配置自动输出、死区时间、lock级别 */
  TIM_BDTRInitStructure.TIM_OSSRState = TIM_OSSRState_Enable;
  TIM_BDTRInitStructure.TIM_OSSIState = TIM_OSSIState_Enable;
  TIM_BDTRInitStructure.TIM_LOCKLevel = TIM_LOCKLevel_OFF;
  TIM_BDTRInitStructure.TIM_DeadTime  = 10;            //死区时间10
  TIM_BDTRInitStructure.TIM_Break     = TIM_Break_Disable;
  TIM_BDTRInitStructure.TIM_BreakPolarity   = TIM_BreakPolarity_High;
  TIM_BDTRInitStructure.TIM_AutomaticOutput = TIM_AutomaticOutput_Enable;
  TIM_BDTRConfig(TIM1, &TIM_BDTRInitStructure);

	/* 关闭输出 */
  CLOSE_U_H;CLOSE_U_L;
  CLOSE_V_H;CLOSE_V_L;
  CLOSE_W_H;CLOSE_W_L;

//  TIM_SelectSlaveMode(TIM1,TIM_SlaveMode_Trigger);  //设置为从模式
  
	/* 打开timer */
	TIM_CCPreloadControl(TIM1, DISABLE); //TIM_CCPreloadControl(TIM1, ENABLE);       //使能预装载
	TIM_ITConfig(TIM1, TIM_IT_COM, ENABLE);   //	TIM_ITConfig(TIM1, TIM_IT_COM, ENABLE);   //配置中断
  TIM_Cmd(TIM1, ENABLE); //TIM_Cmd(TIM1, DISABLE);  //TIM_Cmd(TIM1, ENABLE);                    //使能timer counter
	TIM_CtrlPWMOutputs(TIM1, ENABLE);        //  TIM_CtrlPWMOutputs(TIM1, ENABLE);         //打开PWM输出
  
	/* 打开NVIC中断 */
	NVIC_InitStructure.NVIC_IRQChannelPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStructure.NVIC_IRQChannel = TIM1_BRK_UP_TRG_COM_IRQn;//NVIC_InitStructure.NVIC_IRQChannel = TIM1_UP_TRG_COM_IRQn;
	NVIC_Init(&NVIC_InitStructure);
#endif
}


/**********************************************************************
 * @bref   init pwm hardware
 * @param  
 * @note   
**********************************************************************/
void drv_init_pwm(void)
{
  drv_init_pwm_gpio();
	drv_init_tim1();
  CLOSE_U_H; CLOSE_V_H; CLOSE_W_H;
  CLOSE_U_L; CLOSE_V_L; CLOSE_W_L;
}

/**********************************************************************
 * @bref   init debug uart1
 * @param  
 * @note   
**********************************************************************/
void drv_init_uart1(void)
{   
  GPIO_InitTypeDef GPIO_InitStructure={0};
	USART_InitTypeDef USART_InitStructure={0};
	
	/* config GPIO clock And config USART1 clock */
	ENABLE_PWM_PORT_CLK();   
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_USART1, ENABLE);

	/* USART1 GPIO config */
	/* Configure USART1 Tx  as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = UART_TX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(UART_TX_PORT, &GPIO_InitStructure);    
	GPIO_PinAFConfig(UART_TX_PORT, UART_TX_PIN_SOURCE, GPIO_AF_1);
	
	/* Configure USART1 Rx  as input floating */
//	GPIO_InitStructure.GPIO_Pin = USART1_RX_PIN;
//	GPIO_Init(USART1_RX_PORT, &GPIO_InitStructure);	
//	GPIO_PinAFConfig(USART1_RX_PORT,USART1_RX_PIN_SOURCE,GPIO_AF_1);
	
	/* USART1  mode config */
  USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = /*USART_Mode_Rx | */USART_Mode_Tx;
 	USART_Init(USART1 , &USART_InitStructure);  
  /* Enable USART */
  USART_Cmd(USART1, ENABLE);
  /* Enable the USARTy Interrupt */
}


/**********************************************************************
 * @bref   初始化端口(led、hall电源开关、按键等)
 * @param  
 * @note   
**********************************************************************/
void drv_init_gpio(void)
{
  /* LED端口初始化 */
  RED_OFF;GREEN_OFF;
  LED1_OFF;LED2_OFF;LED3_OFF;LED4_OFF;
  
  gpio_config(RED_PORT, RED_PIN, GPIO_Mode_OUT, GPIO_PuPd_UP);
  gpio_config(GREEN_PORT, GREEN_PIN, GPIO_Mode_OUT, GPIO_PuPd_UP);
  gpio_config(LED1_PORT, LED1_PIN, GPIO_Mode_OUT, GPIO_PuPd_UP);
  gpio_config(LED2_PORT, LED2_PIN, GPIO_Mode_OUT, GPIO_PuPd_UP);
  gpio_config(LED3_PORT, LED3_PIN, GPIO_Mode_OUT, GPIO_PuPd_UP);
  gpio_config(LED4_PORT, LED4_PIN, GPIO_Mode_OUT, GPIO_PuPd_UP);

  /* 配置HALL电源开关 */
  CLOSE_HALL_POWER;
  gpio_config(HALL_POWER_PORT, HALL_POWER_PIN, GPIO_Mode_OUT, GPIO_PuPd_DOWN);   //必须在这个之前初始化HALL三个引脚，否则开电不成功
//  OPEN_HALL_POWER; 
  
  /* 配置STAT充电检测管脚 */
  gpio_config(USB_STAT_PORT, USB_STAT_PIN, GPIO_Mode_IN, GPIO_PuPd_UP);

  /* KEY/USB_DECT初始化 */
  GPIO_InitTypeDef m_gpio;
  EXTI_InitTypeDef m_exti;
  NVIC_InitTypeDef NVIC_InitStructure;
  
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

  /* 配置PA0、PA7 */
  m_gpio.GPIO_Mode  = GPIO_Mode_IN;
	m_gpio.GPIO_OType = GPIO_OType_PP;
	m_gpio.GPIO_Pin   = GPIO_Pin_0;            //PA0
	m_gpio.GPIO_PuPd  = GPIO_PuPd_DOWN;
	m_gpio.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(GPIOA, &m_gpio);
  
  m_gpio.GPIO_Pin   = GPIO_Pin_7;            //PA7
  GPIO_Init(GPIOA, &m_gpio);
  
  /* 配置NVIC中断0 */
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStructure.NVIC_IRQChannel = EXTI0_1_IRQn;
	NVIC_Init(&NVIC_InitStructure);
  
  /* 配置NVIC中断7 */
  NVIC_InitStructure.NVIC_IRQChannel = EXTI4_15_IRQn;
  NVIC_Init(&NVIC_InitStructure);
  
  /* 关闭 */
//  NVIC_EnableIRQ(EXTI0_1_IRQn);
  NVIC_DisableIRQ(EXTI0_1_IRQn);  //关闭外中断0
  NVIC_DisableIRQ(EXTI4_15_IRQn); //关闭外中断7
//  NVIC_EnableIRQ(EXTI4_15_IRQn);
  
  /* 配置外部中断0 */
  m_exti.EXTI_Line = EXTI_Line0;              
	m_exti.EXTI_Mode = EXTI_Mode_Interrupt;      
	m_exti.EXTI_Trigger = EXTI_Trigger_Rising;//EXTI_Trigger_Rising;
	m_exti.EXTI_LineCmd = ENABLE;
	EXTI_Init(&m_exti);
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource0);
	EXTI_ClearITPendingBit(EXTI_Line0);
  
  /* 配置外部中断7 */
  m_exti.EXTI_Line = EXTI_Line7;              
  EXTI_Init(&m_exti);
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource7);
	EXTI_ClearITPendingBit(EXTI_Line0);

}

void drv_init_debug(void)
{
  gpio_config(DEBUG_PORT, DEBUG_PIN, GPIO_Mode_OUT, GPIO_PuPd_UP);
  GPIO_WriteBit(DEBUG_PORT, DEBUG_PIN, Bit_SET);
}




