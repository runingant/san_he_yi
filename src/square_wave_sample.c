#include <stdio.h>
#include <string.h>
#include "square_wave_hardware.h" 
#include "square_wave_sample.h" 

/*=======================================================================
          变量定义
=========================================================================*/
MOTOR_SINGLE_S s_single;     //信号，包含输入信号和板载信号
uint8_t g_sampleChannelNo = 1; //采样状态
uint8_t g_currentSampleSuccess = 0;



/*=======================================================================
          滤波FIFO相关
=========================================================================*/

/**********************************************************************
 * @bref	 初始化一路FIFO
 * @param  
 **********************************************************************/
static void sample_fifo_init(SAMPLE_S *psample, uint32_t maxNumber)
{
  if((psample) && (maxNumber <= FIFO_NUMBER))
  {
    memset(psample, 0x00, sizeof(SAMPLE_S));
    psample->maxNumber = maxNumber;
  }
}

/**********************************************************************
 * @bref	 获取采样信号的有效值
 * @param  
 **********************************************************************/
static inline SAMPLE_S *get_sample_pointer(uint8_t type)
{
	SAMPLE_S *psample = NULL;

	/* 选择类型*/
	switch(type)
	{
		case SAMPLE_VBS:
		{
			psample = &s_single.adcVbus;
			break;
		}
		
		case SAMPLE_CURRENT:
		{
			psample = &s_single.adcCurrent;
			break;
		}
		
		case SAMPLE_INPUT_PWM:
		{
			psample = &s_single.adcInputPwm;
			break;
		}

		case SAMPLE_SPEED:
		{
			psample = &s_single.speed;
			break;
		}
		
		default:
		{
			break;  //错误
		}
	}

	return psample;
}

/**********************************************************************
 * @bref	 滤波及输出
 * @param  type - 类型
 **********************************************************************/
uint32_t sample_get_average_val(uint8_t type)
{
	uint32_t i=0;
	uint32_t ret = 0;
	SAMPLE_S *psample = get_sample_pointer(type);

	if(psample)
	{
		for(i=0; i<psample->maxNumber; i++)
		{
			ret += psample->fifo[i];
		}
		ret /= psample->maxNumber;
	}
	return ret;
}

/**********************************************************************
 * @bref	 滤波及输出
 * @param  psample  - 待处理的buffer指针
 *         val      - 最新值
 *         filterLv - 滤波等级 0-不滤波 1-输出均值
 *         filterDepth - 滤波深度(最近几次的值，取值范围1-FIFO_NUMBER)
 **********************************************************************/
uint32_t sample_filter(uint8_t type, uint32_t val, FILTER_E filterLv, uint8_t filterDepth)
{
	uint32_t i=0;
  uint32_t ret = 0;
	uint32_t index = 0;
  SAMPLE_S *psample = get_sample_pointer(type);

	/* 初始化处理 */
	if((psample) && (psample->initFlag == 0))
	{
		sample_fifo_init(psample, FIFO_NUMBER);  //初始化 FIFO_NUMBER 个元素
		psample->initFlag = 1;
	}

	/* 数据填充 */
  if(  (psample) 
		&& (psample->maxNumber > 0)
		&& (filterDepth != 0)
		&& (filterDepth <= psample->maxNumber) 
		&& (filterLv != NO_FILTER) )
  {
  	/* 存放最新值 */
    psample->index = (psample->index >= psample->maxNumber) ? 0 : psample->index ; 
    psample->fifo[psample->index] = val;
    psample->index++;

		/* 累加数量 */
		if(psample->number < psample->maxNumber)
		{
			psample->number++;
		}

		/* 滤波输出 */
		if(psample->number < filterDepth)
		{
			/* 存储数量不足,直接输出最新值 */
			goto RET1;         
		}
		else
		{
			/* 存储数量足够，从当前位置往前查找filterDepth个数的平均值(滤波) */
			index = psample->index;
	    i = filterDepth;
			while(i--)
			{
				(index == 0) ? (index = psample->maxNumber-1) : (index--);
				ret += psample->fifo[index];
			}
			ret /= filterDepth;
		}

#if 0 
    for(i=0; i<psample->maxNumber; i++)  /* average */
    {
      ret += psample->fifo[i];
    }
    ret /= psample->maxNumber;
#endif
    psample->validValue = ret; 

  }
  else
  {/* 非法直接输出最新值 */
RET1:    
		ret = val;                  
		psample->validValue = ret; 
  }
  return ret;
}


/*=======================================================================
          采样及存储
=========================================================================*/

/**********************************************************************
 * @bref	 处理采样和信号转换问题
 * @param  
 **********************************************************************/
int sample_task(void)
{
  int ret = 0;
  static uint8_t s_lastSampleChannelNo = 0xFF;
  
//  if(s_lastSampleChannelNo != g_sampleChannelNo)
  {
    s_lastSampleChannelNo = g_sampleChannelNo;
    switch(g_sampleChannelNo)
    {
      /* 采样PWM */
      case 0:
        ADC1->CHSELR = ADC_Channel_3;  
        ADC1->SMPR = ADC_SampleTime_55_5Cycles;
        ADC_StartOfConversion(ADC1);
        break;

      /* 采样VBUS */
      case 1:
        ADC1->CHSELR = VBUS_ADC1_CHANNEL;  
        ADC1->SMPR = ADC_SampleTime_55_5Cycles;
        ADC_StartOfConversion(ADC1);
        break;

      /* 采样Current */
      case 2:
        ADC1->CHSELR = CURRENT_ADC1_CHANNEL;  
        ADC1->SMPR = ADC_SampleTime_239_5Cycles;
        ADC_StartOfConversion(ADC1);
        break;

      case 3:
  //			s_single.inputDir = drv_get_direction(); 
  //			s_single.vbus = (sample_get_val(SAMPLE_VBS) * 3300) / 0xFFF;
  //			s_single.pwmPercent = (sample_get_val(SAMPLE_INPUT_PWM) * 3300) / 0xFFF;
  //			s_single.current = (sample_get_val(SAMPLE_CURRENT) * 3300) / 0xFFF;
  //			printf("vbus = %d, pwm = %d, current = %d\r\n", (uint32_t)s_single.vbus, (uint32_t)s_single.pwmPercent, (uint32_t)s_single.current);//test
        break;

      default: 
        break;
    }
  }
  
  if(g_currentSampleSuccess)
  {
    ret = 1;
  }
  
	return ret;
}


void sample_set_pwmpercent(uint8_t percent)
{
  s_single.adcInputPwm.validValue = percent;
}

/**********************************************************************
 * @bref	 获取最新有效数据
 * @param  
 **********************************************************************/
uint32_t sample_get_val(uint8_t type)
{
	SAMPLE_S *psample = get_sample_pointer(type);

	if(psample)
	{
		return psample->validValue;
	}
	else
	{
		return 0;
	}
}





