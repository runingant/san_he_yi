#include <stdio.h>
#include "sw_tools.h"
#include "platform_service.h" 
#include "square_wave_pid.h"
#include "square_wave_sample.h"
#include "square_wave_control.h"


/*
这个网站不错
http://www.360doc.com/content/19/1114/00/1777922_873043804.shtml

正常换向顺序：AB > AC > BC > BA > CA > CB
反向换向顺序：BA > BC > AC > AB > CB > CA
*/

#define IS1(pControl) (pControl->prevHallState == 1) 
#define IS2(pControl) (pControl->prevHallState == 2) 
#define IS3(pControl) (pControl->prevHallState == 3) 
#define IS4(pControl) (pControl->prevHallState == 4) 
#define IS5(pControl) (pControl->prevHallState == 5) 
#define IS6(pControl) (pControl->prevHallState == 6) 

extern uint8_t g_currentSampleSuccess;
extern uint8_t g_adcConvertionDone;
extern uint8_t runEnable;
extern uint8_t gOverCurrentHappen;

/*=======================================================================
          函数声明
=========================================================================*/

/*=======================================================================
          变量定义
=========================================================================*/
int16_t s_pwmMaxValue     = 0;           //PWM最大占空比
int16_t s_pwmMinValue     = 0;           //PWM最小占空比

/* 切换时死区延时 */
#define SW_DELAY()  __nop()


/* 控制参数 */
SW_CONTROL_S sControl = {
.targetSpeed = 0,
.hallState = 0xAA, 
};

/* PID参数 */
PID pid={
	.Kp = 0.02,
	.Ki = 0.01,
	.Kd = 0.0,
};

/* 电流PID参数 */
PID gCurrentPid = {
	.Kp = 0.002,
	.Ki = 0.001,
	.Kd = 0.0,
};

/**********************************************************************
 * @bref   HALL正转
						 ==============================
					 + 通电顺序 		HALL值(hall顺序321)	 HALL值(hall顺序123)
					 +	 BC 					2 											2									3
					 +	 AC 					3 											6									2
					 +	 AB 					1 											4									6
					 +	 CB 					5 											5									4
					 +	 CA 					4 											1									5
					 +	 BA 					6 											3									1
						=============================
						[正转hall] 2		6  4	5  1	3
						=============================
						[hall顺序321时]
						正转顺序：BA CA CB AB AC BC	 
						反转顺序：AC BC BA CA CB AB
						=============================
						[hall顺序123时]
						正转顺序：AC AB CB CA BA BC 
            反转顺序：BA BC AC AB CB CA
          
 * @param  
 OUTPUT_BC_a()
 OUTPUT_B_ac()
 OUTPUT_AB_c()
 OUTPUT_A_bc()
 OUTPUT_AC_b()
 OUTPUT_C_ab()
**********************************************************************/
static void sw_switch_positive(SW_CONTROL_S *pControl, uint8_t force)
{
	static uint8_t speedConfirmFlag = 0;
	
  switch(pControl->hallState)
  {
    case 2:
    {
    #if (DRIVE_MODE == SAN_SAN)
			OUTPUT_AB_c();
		#else
			OUTPUT_AC();
		#endif
			speedConfirmFlag |= (1<<0);
      break;
    }
    
    case 6:
    {
    #if (DRIVE_MODE == SAN_SAN)
			OUTPUT_A_bc();
		#else
			OUTPUT_AB();
		#endif
			speedConfirmFlag |= (1<<1);
      break;
    }
    
    case 4:  
    {	
    #if (DRIVE_MODE == SAN_SAN)
			OUTPUT_AC_b();
		#else
			OUTPUT_CB();
		#endif
			speedConfirmFlag |= (1<<2);
      break;
    }

    case 5:
    {
    #if (DRIVE_MODE == SAN_SAN)
			OUTPUT_C_ab();
		#else
			OUTPUT_CA();
		#endif
			speedConfirmFlag |= (1<<3);
      break;
    }
    
    case 1:
    {
    #if (DRIVE_MODE == SAN_SAN)
			OUTPUT_BC_a();
		#else
			OUTPUT_BA();
		#endif
			speedConfirmFlag |= (1<<4);
      break;
    }
    
    case 3:
    {
    #if (DRIVE_MODE == SAN_SAN)
			OUTPUT_B_ac();
		#else
			//OPEN_V_H;delay_01ms(5);
			OUTPUT_BC();
		#endif
			if((speedConfirmFlag == 0x1F)
				||(speedConfirmFlag == 0x1E)
				||(speedConfirmFlag == 0x1D)
				||(speedConfirmFlag == 0x1C)
				||(speedConfirmFlag == 0x17)
				||(speedConfirmFlag == 0x0F))//全部准备OK 或者 任意丢失1个信号都算正常
			{
				//sw_calculate_speed();
				pControl->speedCounter = drv_get_measureTimer();
				pControl->speedValidCounter = 300;
			}
			speedConfirmFlag = 0;
      break;
    }
    
    default: //其他情况就调整
    {
      break;
    }
  }  
}

/**********************************************************************
 * @bref   HALL反转
           ==============================
         + 通电顺序     HALL值(hall顺序321)   HALL值(hall顺序123)
         +   AB           1                       4
         +   BC           2                       2
         +   AC           3                       6
         +   CA           4                       1
         +   CB           5                       5
         +   BA           6                       3
          =============================
          hall值：  2  6  4  5  1  3
          正转顺序：AC AB CB CA BA BC 
          反转顺序：CA BA BC AC AB CB
                    
 * @param  
**********************************************************************/
static inline bool sw_switch_negtive(SW_CONTROL_S *pControl, uint8_t force)
{
	static uint8_t speedConfirmFlag = 0;

	//3 1 5 4 6 2
  switch(pControl->hallState)
  {
    case 2: 
    {
      OUTPUT_CA();
			speedConfirmFlag |= (1<<0);
      break;
    }
    
    case 6:
    {
      OUTPUT_BA();
			speedConfirmFlag |= (1<<1);
      break;
    }
    
    case 4:  
    {	
      OUTPUT_BC();
			speedConfirmFlag |= (1<<2);
      break;
    }

    case 5:
    {
      OUTPUT_AC();
			speedConfirmFlag |= (1<<3);
			break;
    }
    
    case 1:
    {
      OUTPUT_AB();
			speedConfirmFlag |= (1<<4);
      break;
    }
    
    case 3:  //ca cb
    {
      OUTPUT_CB();
			if((speedConfirmFlag == 0x1F)
				||(speedConfirmFlag == 0x1E)
				||(speedConfirmFlag == 0x1D)
				||(speedConfirmFlag == 0x1C)
				||(speedConfirmFlag == 0x17)
				||(speedConfirmFlag == 0x0F))//全部准备OK 或者 任意丢失1个信号都算正常
			{
      	pControl->speedCounter = drv_get_measureTimer();
		  	pControl->speedValidCounter = 200;
			}
			speedConfirmFlag = 0;
      break;
    }
    
    default: //其他情况就调整
    {
      break;
    }
  }  
  return true;
}

/**********************************************************************
 * @bref	 根据HALL切换
 * @param 
           DEBUG_HIGH();
           delay_01ms(hall_angle_compensation(pControl->nowSpeed));  //角度补偿
           DEBUG_LOW();
 **********************************************************************/ 
void sw_hall_switch(SW_CONTROL_S *pControl, uint8_t force)
{
  pControl->hallState = drv_get_hallstate(); 
#if(BIAO_DING == 1)
	printf("hall = %d\r\n", pControl->hallState);
#else
  
  /* 在启动状态/运行状态/保护状态 或 force强制 几种条件下才允许换向 */
  if((pControl->status == STA_START_UP) 
    || (pControl->status == STA_RUNING) 
    || (pControl->status == STA_OVER_CURRENT) 
    || (pControl->status == STA_PROTECT)
    || (force == 1)) 
  {
    if((pControl->prevHallState != pControl->hallState) || (force == 1))
    {
  //    delay_01ms(15);  //公模12槽电机特有延时，因为hall位置不对
      sw_switch_positive(pControl, force);
//    sw_switch_negtive(pControl, force);  
      pControl->prevHallState = pControl->hallState;
    }
  }
#endif
}


/**********************************************************************
 * @bref   计算速度(每个周期计算1次)
 * @param  
 * @note   可以优化，每个象限切换时候就更新速度
           20200901:电流>1.5A之后，速度滤波层级增加
**********************************************************************/
void sw_calculate_speed(SW_CONTROL_S *pControl)
{
	float time = 0;
	float cycles = 1; //总圈数
	uint32_t speed = 0;

	if(pControl->speedValidCounter)
	{
		if(pControl->speedCounter != pControl->speedCounterPrev)  /* 速度更新才重新计算 */
		{
			time = pControl->speedCounter * MEASURE_TIMER_UNIT; //总时间
			cycles = cycles * CALC_SPEED_FACTOR; //计算1分钟内总圈数
			speed = cycles / time; //速度rpm

      pControl->nowSpeed = sample_filter(SAMPLE_SPEED, speed, AVERAGE_FILTER, FIFO_NUMBER);//速度滤波
			pControl->speedCounterPrev = pControl->speedCounter;
		}
	}
	else
	{
		pControl->nowSpeed = 0;
		pControl->speedCounter = 0;
	}
}

/**********************************************************************
 * @bref	 初始化square_wave_contorl硬件
 * @param  
 **********************************************************************/
void sw_init_hardware(void)
{
	delay_ms(100); //解决上电瞬间抖动   
  drv_init_cpu();
  drv_init_hall();
  drv_init_gpio();
  drv_init_pwm();
  drv_init_adc1();
#if (MARK != MARK_NULL)
  #if(BOARD != BOARD_SAN_HE_YI)
  delay_ms(3000);
  #endif
	drv_init_uart1(); 
  printf("uart init\r\n");
#endif
  
  /* 按键唤醒才初始化看门狗 */
  if((KEY_PRESS) || (USB_DECT_PRESS))
  {
    printf("open wdg;\r\n");
    IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
    IWDG_SetPrescaler(IWDG_Prescaler_32); //设置看门狗时钟
    IWDG_SetReload(1000); //设置重载初始值
    IWDG_WriteAccessCmd(IWDG_WriteAccess_Disable);
    IWDG_Enable();  //使能IWDG
  }
}


void print_motor_info(void)
{
  float temp = 0;
	uint32_t pwminput = 0;

	temp = sControl.sample.fInputPwmPercent * 100;
	pwminput = (uint32_t)temp ;
  printf("PWM = %d, inputPercent=%d, spd=%d, cur=%d, sta=%d\r\n", 
         sControl.pwmValue, 
         pwminput, 
         sControl.nowSpeed, 
         sControl.sample.uCurrentMA, 
         sControl.status); 
}

/**********************************************************************
 * @bref   FlashReadProtecConfig获取UID
 * @param  
 * @note   
   sw_get_temprature_adc_value()
**********************************************************************/
void set_flash_protection(void)
{
	FLASH_Unlock();
	FLASH_OB_Unlock();
	FLASH_OB_RDPConfig(OB_RDP_Level_1);	
	FLASH_OB_Lock();
	FLASH_Lock();
}

/**********************************************************************
 * @bref   电流环，限流8A
 * @param  pControl - 控制指针
 *         current - 目标电流
 * @note   
**********************************************************************/
void current_limit_output(SW_CONTROL_S *pControl, uint16_t current)
{
  int32_t pwm = 0;

	/* 电流环PID计算 */
  pwm = (int32_t)PIDRegulation(current, pControl->sample.uCurrentMA ,&gCurrentPid);
  pControl->pwmValue += pwm;
	
  /* 限幅处理并更新TIMER */
  pControl->pwmValue = LIMIT_MAX(pControl->pwmValue);
  pControl->pwmValue = LIMIT_MIN(pControl->pwmValue);
#if(CONTROL_TYPE == CONTROL_BY_PWM_GPIO)
		SET_TIM_CCR(PWMTIM_PERIOD_CYCLE - pControl->pwmValue); 
#elif(CONTROL_TYPE == CONTROL_BY_GPIO_PWM)
		SET_TIM_CCR(pControl->pwmValue);
#endif
}



/**********************************************************************
 * @bref   根据输入PWM百分比，设置输出值
 * @param  
 * @note   
**********************************************************************/
void pwm_mode_output(SW_CONTROL_S *pControl)
{
	int pwmSet = 0;
  float percent = pControl->sample.fInputPwmPercent;

	if(pControl->controlMode == MODE_PWM)
	{
		/* 计算速度 */
		pControl->targetSpeed = (1-percent) * MOTOR_MAX_SPEED;
		pwmSet = (1-percent) * PWMTIM_PERIOD_CYCLE;
		pControl->pwmValue = pwmSet;
		
		/* 限幅 */
		pControl->pwmValue = LIMIT_MAX(pControl->pwmValue);
		pControl->pwmValue = LIMIT_MIN(pControl->pwmValue);

		/* 更新timer */
#if(CONTROL_TYPE == CONTROL_BY_PWM_GPIO)
	SET_TIM_CCR(PWMTIM_PERIOD_CYCLE - pControl->pwmValue);
#elif(CONTROL_TYPE == CONTROL_BY_GPIO_PWM)
  SET_TIM_CCR(pControl->pwmValue);
#endif
	}
}

/**********************************************************************
 * @bref	 计算PID
 * @param  
 **********************************************************************/
void auto_mode_output(SW_CONTROL_S *pControl)
{
	int32_t pwm = 0;
	float percent = pControl->sample.fInputPwmPercent;

	if((pControl->controlMode == MODE_AUTO)
    &&((pControl->status == STA_START_UP) || (pControl->status == STA_RUNING))
    && (gOverCurrentHappen == 0))
	{
		/* 导入百分比数据 */
		pControl->targetSpeed = percent * MOTOR_MAX_SPEED;  //根据输入PWM设置目标转速
    
	  /* PID计算 */
		pwm = (int32_t)PIDRegulation(pControl->targetSpeed, pControl->nowSpeed, &pid);
		//pwm = simple_pid(pControl->targetSpeed, pControl->nowSpeed);
	  pControl->pwmValue += pwm;

		/* 限幅 */
		pControl->pwmValue = LIMIT_MAX(pControl->pwmValue);
	  pControl->pwmValue = LIMIT_MIN(pControl->pwmValue);

		/* 更新TIMER */
#if(CONTROL_TYPE == CONTROL_BY_PWM_GPIO)
		//SET_TIM_CCR(PWMTIM_PERIOD_CYCLE - pControl->pwmValue);
    SET_TIM_CCR(pControl->pwmValue);
#elif(CONTROL_TYPE == CONTROL_BY_GPIO_PWM)
		SET_TIM_CCR(pControl->pwmValue);
#endif
	}
}

/**********************************************************************
 * @bref	 设置工作模式
 * @param  
 **********************************************************************/
void set_control_mode(SW_CONTROL_S* pControl, uint8_t mode)
{
	pControl->controlMode = mode;
}

void set_input_pwm_percent(float percent)
{
  if((percent <= 1) && (percent >= 0))
  {
    sControl.sample.fInputPwmPercent = percent;
  }
}

float get_vbat_voltage(void)
{
  return sControl.sample.uVBus;
}

float get_motor_current(void)
{
  return sControl.sample.uCurrentMA;
}

/**********************************************************************
 * @bref	 printf重定义
 * @param  
 **********************************************************************/
int fputc(int ch, FILE *f)
{
#if (MARK != MARK_NULL)
		/* 发送一个字节数据到串口 */
		USART_SendData(USART1, (uint8_t) ch);
		
		/* 等待发送完毕 */
		while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);		
#endif
		return (ch);
}
