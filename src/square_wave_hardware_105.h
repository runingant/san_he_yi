#ifndef __square_wave_hardware_105_h__
#define __square_wave_hardware_105_h__

#include <stdint.h>
#include "hk32f0301m_gpio.h"

#define EXCHANGE_UV            (0)

#define HIGH_OPEN              1  //导通时电平为高
#define LOW_OPEN               0  //导通时电平为低

/* 上下桥开通方式 */
#define P_MOS_MODE        HIGH_OPEN              //P MOS(上桥) 低导通
#define N_MOS_MODE        HIGH_OPEN              //N MOS(下桥) 低导通

/* PMOS打开方式 */
#if (P_MOS_MODE == HIGH_OPEN)
	#define GH_SET				 Bit_SET						 //GPIO模式下 P管GPIO 高导通
	#define GH_RESET			 Bit_RESET 
	#define PH_SET				 Bit_SET						 //PWM模式下 P管GPIO 高导通
	#define PH_RESET			 Bit_RESET 
#else
	#define GH_SET				 Bit_RESET					 //GPIO模式下 P管GPIO 低导通
	#define GH_RESET			 Bit_SET 
	#define PH_SET				 Bit_RESET					 //PWM模式下 P管GPIO 低导通
	#define PH_RESET			 Bit_SET 
#endif

/* NMOS打开方式 */
#if (N_MOS_MODE == HIGH_OPEN)
	#define GL_SET			 Bit_SET						//N管GPIO 高导通
	#define GL_RESET		 Bit_RESET 
	#define NMOS_DEFAULT_VALUE         TIM_OCNPolarity_High   //N管高导通
#else
	#define GL_SET			 Bit_RESET					//N管GPIO 低导通
	#define GL_RESET		 Bit_SET 
	#define NMOS_DEFAULT_VALUE         TIM_OCNPolarity_Low    //N管低导通
#endif

/* 控制方式选择 */
#define CONTROL_BY_GPIO        (1)  //废弃
#define CONTROL_BY_PWM         (2)  //废弃
#define CONTROL_BY_PWM_GPIO    (3)  //P管PWM
#define CONTROL_BY_GPIO_PWM    (4)  //N管PWM
#define CONTROL_TYPE           CONTROL_BY_PWM_GPIO
/*=======================================================================
          UVW 硬件端口定义,
          UH - PD1 
          UL - PA1
          VH - PC7
          VL - PA2
          WH - PD7
          WL - PA3
=========================================================================*/

/* 实际相序与图纸相反，图纸的U和W交换 
      图纸                 交换后
U     PD1、PA1             PC3、PA3
V     PC7、PA2             PC7、PA2
W     PC3、PA3             PD1、PA1
*/
#define PWM_CH1_PORT              GPIOA               //PA1
#define PWM_CH1_PIN               GPIO_Pin_1          
#define PWM_CH1_PIN_SOURCE        GPIO_PinSource1
#define PWM_CH2_PORT              GPIOA               //PA2
#define PWM_CH2_PIN               GPIO_Pin_2
#define PWM_CH2_PIN_SOURCE        GPIO_PinSource2
#define PWM_CH3_PORT              GPIOA               //PA3
#define PWM_CH3_PIN               GPIO_Pin_3
#define PWM_CH3_PIN_SOURCE        GPIO_PinSource3

#define PWM_CH1N_PORT             GPIOD               //PD1
#define PWM_CH1N_PIN              GPIO_Pin_1
#define PWM_CH1N_PIN_SOURCE       GPIO_PinSource1
#define PWM_CH2N_PORT             GPIOC               //PC7
#define PWM_CH2N_PIN              GPIO_Pin_7
#define PWM_CH2N_PIN_SOURCE       GPIO_PinSource7
#define PWM_CH3N_PORT             GPIOC               //PC3
#define PWM_CH3N_PIN              GPIO_Pin_3
#define PWM_CH3N_PIN_SOURCE       GPIO_PinSource3


/*=======================================================================
          HALL 硬件端口定义
          Hall1 - PD6
          Hall2 - PD7
          Hall3 - PC6
=========================================================================*/
#define HALL_1_PORT               GPIOD
#define HALL_1_PIN                GPIO_Pin_6  
#define HALL_1_PIN_SOURCE         GPIO_PinSource6   
#define HALL_1_CLK                xxxx

#define HALL_2_PORT               GPIOD
#define HALL_2_PIN                GPIO_Pin_7 
#define HALL_2_PIN_SOURCE         GPIO_PinSource7
#define HALL_2_CLK                xxxx

#define HALL_3_PORT               GPIOC
#define HALL_3_PIN                GPIO_Pin_6 
#define HALL_3_PIN_SOURCE         GPIO_PinSource6
#define HALL_3_CLK                xxxx

/*=======================================================================
          ADC 硬件端口定义
          VBUS - PD4?
=========================================================================*/
#define VBUS_PORT               GPIOD
#define VBUS_PIN                GPIO_Pin_3
#define VBUS_PIN_SOURCE         GPIO_PinSource3
#define VBUS_PIN_AD_CHANNEL     ADC_Channel_3 

#define CURR1_PORT              GPIOD
#define CURR1_PIN               GPIO_Pin_2
#define CURR1_PIN_SOURCE        GPIO_PinSource2
#define CURR1_PIN_AD_CHANNEL    ADC_Channel_4 

#define PWM_IN_PORT             GPIOC
#define PWM_IN_PIN              GPIO_Pin_4
#define PWM_IN_PIN_SOURCE       GPIO_PinSource4
#define PWM_IN_AD_CHANNEL       ADC_Channel_2 

/*=======================================================================
          DEBUG管脚
=========================================================================*/
#define DEBUG_PORT               GPIOB
#define DEBUG_PIN                GPIO_Pin_4
#define DEBUG_PIN_SOURCE         GPIO_PinSource4 

#define DEBUG_HIGH()             GPIO_WriteBit(DEBUG_PORT, DEBUG_PIN, Bit_SET)
#define DEBUG_LOW()              GPIO_WriteBit(DEBUG_PORT, DEBUG_PIN, Bit_RESET)

/*=======================================================================
          控制切换
-----------GPIO/PWM控制--------------------------------------------------
          上桥(P管) - GPIO控制
          下桥(N管) - PWM控制
=========================================================================*/

#if(CONTROL_TYPE==CONTROL_BY_GPIO)     
	/* P管 */
  #define OPEN_U_H    GPIO_WriteBit(PWM_CH1_PORT, PWM_CH1_PIN, GH_SET)
  #define CLOSE_U_H   GPIO_WriteBit(PWM_CH1_PORT, PWM_CH1_PIN, GH_RESET)
  #define OPEN_V_H    GPIO_WriteBit(PWM_CH2_PORT, PWM_CH2_PIN, GH_SET)
  #define CLOSE_V_H   GPIO_WriteBit(PWM_CH2_PORT, PWM_CH2_PIN, GH_RESET)
  #define OPEN_W_H    GPIO_WriteBit(PWM_CH3_PORT, PWM_CH3_PIN, GH_SET)
  #define CLOSE_W_H   GPIO_WriteBit(PWM_CH3_PORT, PWM_CH3_PIN, GH_RESET)
	#define DISABLE_UVW_H do{CLOSE_U_H;CLOSE_V_H;CLOSE_W_H;}while(0)   //关闭所有UVW_H输出
  
  /* N管 */
  #define OPEN_U_L    GPIO_WriteBit(PWM_CH1N_PORT, PWM_CH1N_PIN, GL_SET)
  #define CLOSE_U_L   GPIO_WriteBit(PWM_CH1N_PORT, PWM_CH1N_PIN, GL_RESET)
  #define OPEN_V_L    GPIO_WriteBit(PWM_CH2N_PORT, PWM_CH2N_PIN, GL_SET)
  #define CLOSE_V_L   GPIO_WriteBit(PWM_CH2N_PORT, PWM_CH2N_PIN, GL_RESET)
  #define OPEN_W_L    GPIO_WriteBit(PWM_CH3N_PORT, PWM_CH3N_PIN, GL_SET)
  #define CLOSE_W_L   GPIO_WriteBit(PWM_CH3N_PORT, PWM_CH3N_PIN, GL_RESET)
	#define DISABLE_UVW_L do{CLOSE_U_L;CLOSE_V_L;CLOSE_W_L;}while(0)  //关闭所有UVW_L输出

#elif(CONTROL_TYPE == CONTROL_BY_PWM_GPIO)     
  /* P管 */
  #define OPEN_U_H    TIM_CCxNCmd(TIM1, TIM_Channel_1, TIM_CCxN_Enable)   //PA1 - TIM1-CH1N
  #define CLOSE_U_H   TIM_CCxNCmd(TIM1, TIM_Channel_1, TIM_CCxN_Disable)
  #define OPEN_V_H    TIM_CCxNCmd(TIM1, TIM_Channel_2, TIM_CCxN_Enable)   //PA2 - TIM1-CH2N
  #define CLOSE_V_H   TIM_CCxNCmd(TIM1, TIM_Channel_2, TIM_CCxN_Disable)
  #define OPEN_W_H    TIM_CCxNCmd(TIM1, TIM_Channel_3, TIM_CCxN_Enable)   //PA3 - TIM1-CH3N
  #define CLOSE_W_H   TIM_CCxNCmd(TIM1, TIM_Channel_3, TIM_CCxN_Disable)
	#define DISABLE_UVW_H TIM_CtrlPWMOutputs(TIM1, DISABLE)        //关闭所有UVW_L输出

	/* N管 */
  #define OPEN_U_L    GPIO_WriteBit(PWM_CH1N_PORT, PWM_CH1N_PIN, GL_SET)
  #define CLOSE_U_L   GPIO_WriteBit(PWM_CH1N_PORT, PWM_CH1N_PIN, GL_RESET)
  #define OPEN_V_L    GPIO_WriteBit(PWM_CH2N_PORT, PWM_CH2N_PIN, GL_SET)
  #define CLOSE_V_L   GPIO_WriteBit(PWM_CH2N_PORT, PWM_CH2N_PIN, GL_RESET)
  #define OPEN_W_L    GPIO_WriteBit(PWM_CH3N_PORT, PWM_CH3N_PIN, GL_SET)
  #define CLOSE_W_L   GPIO_WriteBit(PWM_CH3N_PORT, PWM_CH3N_PIN, GL_RESET)
	#define DISABLE_UVW_L do{CLOSE_U_L;CLOSE_V_L;CLOSE_W_L;}while(0)  //关闭所有UVW_L输出

#elif(CONTROL_TYPE == CONTROL_BY_GPIO_PWM)
	/* P管 */
  #define OPEN_U_H    GPIO_WriteBit(PWM_CH1_PORT, PWM_CH1_PIN, PH_SET)  //PA1
  #define CLOSE_U_H   GPIO_WriteBit(PWM_CH1_PORT, PWM_CH1_PIN, PH_RESET)
  #define OPEN_V_H    GPIO_WriteBit(PWM_CH2_PORT, PWM_CH2_PIN, PH_SET)
  #define CLOSE_V_H   GPIO_WriteBit(PWM_CH2_PORT, PWM_CH2_PIN, PH_RESET)
  #define OPEN_W_H    GPIO_WriteBit(PWM_CH3_PORT, PWM_CH3_PIN, PH_SET)
  #define CLOSE_W_H   GPIO_WriteBit(PWM_CH3_PORT, PWM_CH3_PIN, PH_RESET)
	#define DISABLE_UVW_H do{CLOSE_U_H;CLOSE_V_H;CLOSE_W_H;}while(0)   //关闭所有UVW_H输出
  
  /* N管 */
  #define OPEN_U_L    TIM_CCxCmd(TIM1, TIM_Channel_1, TIM_CCx_Enable)   //PD1 - TIM1-CH1
  #define CLOSE_U_L   TIM_CCxCmd(TIM1, TIM_Channel_1, TIM_CCx_Disable)
  #define OPEN_V_L    TIM_CCxCmd(TIM1, TIM_Channel_2, TIM_CCx_Enable)
  #define CLOSE_V_L   TIM_CCxCmd(TIM1, TIM_Channel_2, TIM_CCx_Disable)
  #define OPEN_W_L    TIM_CCxCmd(TIM1, TIM_Channel_3, TIM_CCx_Enable)
  #define CLOSE_W_L   TIM_CCxCmd(TIM1, TIM_Channel_3, TIM_CCx_Disable)
	#define DISABLE_UVW_L TIM_CtrlPWMOutputs(TIM1, DISABLE)        //关闭所有UVW_L输出
#endif

#endif
