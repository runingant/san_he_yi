#include "sw_tools.h"
#include "square_wave_control.h"
 
#define auto_delay()  delay_ms(10)

const char *stepTable[7]={"null","AC","AB","CB","CA","BA","BC"};

/**********************************************************************
 * @bref   标定程序
 * @param  null
 * @note   开环运行，并在中断实时打印hall值 
 **********************************************************************/ 
void calibration_task(void)
{
	static uint8_t run_step = 0;

	/* 换向切换步骤 */
	run_step = (run_step>=6)?1:(++run_step);
	printf("drive = %s\r\n", stepTable[run_step]);
	switch(run_step)
  {
    case 1:
      OUTPUT_AC();
      break;
    
    case 2:
      OUTPUT_AB();
      break;
    
    case 3:  
      OUTPUT_CB();
      break;

    case 4:
      OUTPUT_CA();
      break;
    
    case 5:
      OUTPUT_BA();
      break;
    
    case 6:
      OUTPUT_BC(); 
      break;
    
    default:break;
  }  
	
	/* 换向后的延时 */
//	delay_ms(10);
//	CLOSE_ABC();
  delay_ms(10);
}


/* 测试代码 
        HALL321   HALL123   毅瑞德电机
   BC    2          2         6/2
   AC    3          6         2   X
   AB    1          4         1
   CB    5          5         1/5
   CA    4          1         5/4
   BA    6          3         6
*/
static uint8_t s_autoState = 1;
uint8_t gNowHall = 0;
uint8_t sruntimes = 0;
void calibration_task2(void)
{
  sruntimes++;
  if(sruntimes >= 1)
  {
    sruntimes = 0;
    s_autoState++;
  }
	//s_autoState++;
	if(s_autoState > 6)
	{
		s_autoState = 1;
	}

  switch(s_autoState)
  {
    case 1:
    {
      OUTPUT_AC();
      break;
    }
    
    case 2:
    {
      OUTPUT_AB();
      break;
    }
    
    case 3:  
    {	
      OUTPUT_CB();
      break;
    }

    case 4:
    {
      OUTPUT_CA();
      break;
    }
    
    case 5:
    {
      OUTPUT_BA();
      break;
    }
    
    case 6:
    {
      OUTPUT_BC(); 
      break;
    }
    
    default: //其他情况就调整
    {
      break;
    }
  }  
  //s_autoState  = 0;
	delay_ms(1000);
	
  gNowHall = drv_get_hallstate();
  printf("s_autoState=%d, hall = %d\r\n", s_autoState, gNowHall);
	CLOSE_ABC();
	delay_ms(1000);
}


/**********************************************************************
 * @bref	 标定函数
 * @param  标定原理
   开环转起来，然后去测量hall的相序
 **********************************************************************/
void calibration(void)
{
  //1.激励
  //2.响应
  
  //相序1
}



