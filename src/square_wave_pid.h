#ifndef __square_wave_pid_h__ 
#define __sqaure_wave_pid_h__

#include <stdio.h>
#include <stdint.h>
#include <string.h>

/* 定义PID控制结构体 */
typedef struct
{
  float setpoint;      //设定值
  float Kp;            //比例系数
  float Ki;            //微分系数
  float Kd;            //积分系数
 
  float lasterror;     //前一拍偏差   
  float preerror;      //前两拍偏差
  float result;        //输出值
}PID;


void PIDSetResult(PID *vPID, float result);
void PIDSetPID(PID *vPID, float Kp, float Ki, float Kd);
float PIDRegulation(int16_t SetValue, int16_t ActualValue, PID *vPID);

#endif
