#include "debug.h" 


/*=======================================================================
          参数定义
=========================================================================*/
int32_t s_debugTable[3][DEBUG_LOG_NUM] = {0};   //存储BUFFER
int     s_debugNumber = 0;                      //已存储数量
int     s_debugIndex = 0;                       //当前存储位置
uint8_t s_debugLock = 0;                        //lock标志


void debug_delay(uint32_t dd)
{
  int i;
  while(dd--)
  {
    for(i=0; i<1000; i++);
  }
}

void delay_ms(uint16_t m)
{
  int i = 0;
  
  while(m--)
  {
    for(i=0;i<1000;i++);
  }
}

void delay_02ms(uint16_t m)
{
  int i = 0;
  
  while(m--)
  {
    for(i=0;i<10;i++);
  }
}


/**************************************************************************************
*Function   :debug_save
*Description:????log
*Input      :
*Output     :
*Return     :
*Date       :2020-1-14
*Others     :
**************************************************************************************/
void debug_save_log(int32_t a, int32_t b, int32_t c, int32_t d)
{
	if((s_debugLock == 0) && (s_debugNumber < DEBUG_LOG_NUM))
	{
		s_debugTable[0][s_debugIndex] = a;
		s_debugTable[1][s_debugIndex] = b;
		s_debugTable[2][s_debugIndex] = c;
//		s_debugTable[3][s_debugIndex] = d;
		s_debugIndex++;
		s_debugNumber++;
	}
}

/**************************************************************************************
*Function   :debug_print_log
*Description:????log
*Input      :
*Output     :
*Return     :
*Date       :2020-1-14
*Others     :
**************************************************************************************/
void debug_print_log(void)
{
	uint16_t i = 0;

	if(s_debugNumber >= DEBUG_LOG_NUM)
	{
		s_debugLock = 1;
		
		for(i=0; i<DEBUG_LOG_NUM; i++)	 
		{
//			printf("a=%d, b=%d, c=%d, d=%d\r\n", s_debugTable[0][i],
//			                                     s_debugTable[1][i],
//			                                     s_debugTable[2][i],
//			                                     s_debugTable[3][i]/*g_hall.measuredSpeed*/);  
      printf("a=%d, %b=%d, c=%d\r\n", s_debugTable[0][i], s_debugTable[1][i], s_debugTable[2][i]);
		}

		s_debugNumber = 0;
		s_debugIndex = 0;
		
//		printf("==================================\r\n");
//		printf("============== START =============\r\n");
//		printf("==================================\r\n");
    printf("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
    debug_delay(10000); 
    s_debugLock = 0;  //???? ??
	}
}


#if 0
/**
 * @bref   6步换向控制切换
 *  切换方法：根据HALL位置执行切换
 * @param  
 */
uint8_t gsequence_step = 1;
void sw_switch_sequence(void)
{
  uint8_t control = 0;//test
  uint8_t hallState = drv_get_hallstate();

  sControl.hallState = hallState;
  control = sControl.hallState;


    switch(gsequence_step)
    {
      case 1: 
      {
      	CLOSE_U_H;CLOSE_U_L;CLOSE_V_H;CLOSE_V_L;CLOSE_W_H;CLOSE_W_L;
				OPEN_V_L;      //B-
				OPEN_U_H;      //A+
				gsequence_step = 2;
        break;
      }
      
      case 2:
      {
				CLOSE_U_H;CLOSE_U_L;CLOSE_V_H;CLOSE_V_L;CLOSE_W_H;CLOSE_W_L;
				OPEN_W_L;      //C-
				OPEN_U_H;      //A+
				gsequence_step = 3;
        break;
      }
      
      case 3:  
      {	
      	CLOSE_U_H;CLOSE_U_L;CLOSE_V_H;CLOSE_V_L;CLOSE_W_H;CLOSE_W_L;

				OPEN_W_L;      //C-
				OPEN_V_H;      //B+
				gsequence_step = 4;
        break;
      }

      case 4:
      {
				CLOSE_U_H;CLOSE_U_L;CLOSE_V_H;CLOSE_V_L;CLOSE_W_H;CLOSE_W_L;

				OPEN_U_L;      //A-
				OPEN_V_H;      //B+
				gsequence_step = 5;
        break;
      }
      
      case 5:
      {
      	CLOSE_U_H;CLOSE_U_L;CLOSE_V_H;CLOSE_V_L;CLOSE_W_H;CLOSE_W_L;    
				OPEN_U_L;      //A-
				OPEN_W_H;      //C+
				gsequence_step = 6;
        break;
      }
      
      case 6:
      {
				CLOSE_U_H;CLOSE_U_L;CLOSE_V_H;CLOSE_V_L;CLOSE_W_H;CLOSE_W_L;

			  OPEN_V_L;      //B-
				OPEN_W_H;      //C+
				gsequence_step = 1;
        break;
      }
      
      default: //其他情况就调整
      {
        break;
      }
  }

#if (MARK == RECORD_HALL_IRQ)
	debug_save_log(gsequence_step, 0, sControl.hallState, sControl.counters_01ms_all);//test
#endif
	lastState = control;
}

void sw_switch_hall(void)
{
}
#endif

