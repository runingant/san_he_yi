/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
**/
#include <stdio.h>
#include <stdarg.h>
#include "sw_tools.h"
#include "platform_service.h" 
#include "square_wave_pid.h"
#include "square_wave_sample.h"
#include "square_wave_control.h"

#include "application.h"
#include "main.h"

/**********************************************************************
 * @bref   控制任务
 * @param  
 **********************************************************************/
void control_task(SW_CONTROL_S *pControl)
{
#if (BIAO_DING == 1)
		calibration_task();
#else
    if(sample_task())
    {
      sw_status(pControl);
    }
//    debug_print_log();
		delay_ms(1);
#endif
}

/**********************************************************************
 * @bref   sw task
 * @param  
 * @note   
   sw_get_temprature_adc_value()
**********************************************************************/
void print_task(void)
{
//  print_motor_info(); 
  print_app_info();
}

/**********************************************************************
 * @bref   主函数
 * @param  
 * @note   
 **********************************************************************/
int main(void) 
{
	SW_CONTROL_S *pControl = &sControl;

  set_flash_protection(); 
  sw_init_hardware();  //硬件初始化
  platform_init();     //OS初始化
  application_init();  //应用初始化 
	set_control_mode(pControl, MODE_AUTO);//MODE_PWM MODE_AUTO
  platform_create_task(TIMER_CONTINUE, 1, 100, print_task);
  platform_create_task(TIMER_CONTINUE, 2, 10,  driver_task);
  platform_create_task(TIMER_CONTINUE, 3, 180, application_task);
  while(1)
  {
    control_task(pControl); //主循环执行控制任务，是为了让控制任务达到最快的运行速度 
    platform_server();
  }
}

#ifdef  USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
}
#endif


