#ifndef __square_wave_sample_h__
#define __square_wave_sample_h__

#include <stdio.h> 
#include <stdint.h> 
 
#define FIFO_NUMBER 20

typedef enum
{
	NO_FILTER,
	AVERAGE_FILTER,
}FILTER_E;

typedef struct sample_s
{
	uint32_t initFlag;           //初始化标志
  uint32_t fifo[FIFO_NUMBER];  //FIFO
  uint32_t index;              //索引位置
  uint32_t number;             //当前总数
  uint32_t maxNumber;          //最大数量
  
  uint32_t validValue;         //处理后的有效值
}SAMPLE_S;

typedef struct motor_single_s
{
  SAMPLE_S adcInputPwm;      //PWM输入百分比
  SAMPLE_S adcVbus;          //VBUS ADC值
  SAMPLE_S adcCurrent;       //温度 ADC值
  
  SAMPLE_S speed;            //速度滤波
  
  uint8_t inputDir;          //运行方向
  float    vbus;             //VBUS电压
  float    current;          //电流
  float    pwmPercent;            //输入百分比
}MOTOR_SINGLE_S;   


enum
{
  SAMPLE_VBS,
  SAMPLE_CURRENT,
  SAMPLE_INPUT_PWM,
  SAMPLE_SPEED,
};

int sample_task(void);
void sample_set_pwmpercent(uint8_t percent);
uint32_t sample_filter(uint8_t type, uint32_t val, FILTER_E filterLv, uint8_t filterDepth);
uint32_t sample_get_val(uint8_t type);
uint32_t sample_get_average_val(uint8_t type); //not used



#endif
