#ifndef __square_wave_hardware_shy_h__
#define __square_wave_hardware_shy_h__

#include <stdint.h>
#include "hk32f0xx_gpio.h"

#define EXCHANGE_UV            (0)

#define HIGH_OPEN              1  //导通时电平为高
#define LOW_OPEN               0  //导通时电平为低

/* 上下桥开通方式 */
#define P_MOS_MODE        HIGH_OPEN          //P MOS(上桥) 高导通
#define N_MOS_MODE        HIGH_OPEN          //N MOS(下桥) 高导通

/* PMOS打开方式 */
#if (P_MOS_MODE == HIGH_OPEN)
	#define GH_SET				 Bit_SET						 //GPIO模式下 P管GPIO 高导通
	#define GH_RESET			 Bit_RESET 
	#define PH_SET				 Bit_SET						 //PWM模式下 P管GPIO 高导通
	#define PH_RESET			 Bit_RESET 
#else
	#define GH_SET				 Bit_RESET					 //GPIO模式下 P管GPIO 低导通
	#define GH_RESET			 Bit_SET 
	#define PH_SET				 Bit_RESET					 //PWM模式下 P管GPIO 低导通
	#define PH_RESET			 Bit_SET 
#endif

/* NMOS打开方式 */
#if (N_MOS_MODE == HIGH_OPEN)
	#define GL_SET			 Bit_SET						//N管GPIO 高导通
	#define GL_RESET		 Bit_RESET 
	#define NMOS_DEFAULT_VALUE         TIM_OCNPolarity_High   //N管高导通
#else
	#define GL_SET			 Bit_RESET					//N管GPIO 低导通
	#define GL_RESET		 Bit_SET 
	#define NMOS_DEFAULT_VALUE         TIM_OCNPolarity_Low    //N管低导通
#endif

/* 控制方式选择 */
#define CONTROL_BY_GPIO        (1)  //废弃
#define CONTROL_BY_PWM         (2)  //废弃
#define CONTROL_BY_PWM_GPIO    (3)  //P管PWM
#define CONTROL_BY_GPIO_PWM    (4)  //N管PWM  030不支持N管PWM
#define CONTROL_TYPE           CONTROL_BY_PWM_GPIO
/*=======================================================================
          UVW 硬件端口定义,
          UH/UL - PA8/PB1
          VH/VL - PA9/PA11
          WH/WL - PA10/PA12
          使用TIMER15的CH1、CH2、CH3
=========================================================================*/
#define PWM_CH1_PORT              GPIOA             
#define PWM_CH1_PIN               GPIO_Pin_8          
#define PWM_CH1_PIN_SOURCE        GPIO_PinSource8
#define PWM_CH2_PORT              GPIOA              
#define PWM_CH2_PIN               GPIO_Pin_9
#define PWM_CH2_PIN_SOURCE        GPIO_PinSource9
#define PWM_CH3_PORT              GPIOA             
#define PWM_CH3_PIN               GPIO_Pin_10
#define PWM_CH3_PIN_SOURCE        GPIO_PinSource10

#define PWM_CH1N_PORT             GPIOB              
#define PWM_CH1N_PIN              GPIO_Pin_1
#define PWM_CH1N_PIN_SOURCE       GPIO_PinSource1
#define PWM_CH2N_PORT             GPIOA             
#define PWM_CH2N_PIN              GPIO_Pin_11
#define PWM_CH2N_PIN_SOURCE       GPIO_PinSource11
#define PWM_CH3N_PORT             GPIOA             
#define PWM_CH3N_PIN              GPIO_Pin_12
#define PWM_CH3N_PIN_SOURCE       GPIO_PinSource12


/*=======================================================================
          HALL 硬件端口定义
          Hall1 - PB4
          Hall2 - PB5
          Hall3 - PB0
          使用TIMER3的CH1、CH2、CH3
=========================================================================*/
#define HALL_1_PORT               GPIOB
#define HALL_1_PIN                GPIO_Pin_4  
#define HALL_1_PIN_SOURCE         GPIO_PinSource4   
#define HALL_1_CLK                xxxx

#define HALL_2_PORT               GPIOB
#define HALL_2_PIN                GPIO_Pin_5 
#define HALL_2_PIN_SOURCE         GPIO_PinSource5
#define HALL_2_CLK                xxxx

#define HALL_3_PORT               GPIOB
#define HALL_3_PIN                GPIO_Pin_0
#define HALL_3_PIN_SOURCE         GPIO_PinSource0
#define HALL_3_CLK                xxxx

/*=======================================================================
          HALL电源开关
=========================================================================*/
#define HALL_POWER_PORT           GPIOF
#define HALL_POWER_PIN            GPIO_Pin_0
#define OPEN_HALL_POWER           GPIO_WriteBit(HALL_POWER_PORT, HALL_POWER_PIN, Bit_RESET)
#define CLOSE_HALL_POWER          GPIO_WriteBit(HALL_POWER_PORT, HALL_POWER_PIN, Bit_SET)

/*=======================================================================
          采样端口定义
=========================================================================*/
#define CURRENT_PORT           GPIOA
#define CURRENT_PIN            GPIO_Pin_1
#define CURRENT_PIN_SOURCE     GPIO_PinSource1
#define CURRENT_ADC1_CHANNEL   ADC_Channel_1

#define VBUS_PORT              GPIOA
#define VBUS_PIN               GPIO_Pin_3
#define VBUS_PIN_SOURCE        GPIO_PinSource3
#define VBUS_ADC1_CHANNEL      ADC_Channel_3

/*=======================================================================
          KEY管脚
=========================================================================*/
#define KEY_PORT               GPIOA
#define KEY_PIN                GPIO_Pin_0
#define KEY_PIN_SOURCE         GPIO_PinSource0
#define KEY_PRESS              (GPIO_ReadInputDataBit(KEY_PORT, KEY_PIN)==Bit_SET)//读取KEY的电平

#define USB_DECT_PORT          GPIOA
#define USB_DECT_PIN           GPIO_Pin_7
#define USB_DECT_PRESS         (GPIO_ReadInputDataBit(USB_DECT_PORT, USB_DECT_PIN)==Bit_SET) 

/*=======================================================================
          STAT充电状态管脚
=========================================================================*/
#if (BOARD == BOARD_SAN_HE_YI)
	#define USB_STAT_PORT          GPIOA
	#define USB_STAT_PIN           GPIO_Pin_15
#elif (BOARD == BOARD_SAN_HE_YI_V1_2)
//三合一最新V1.2板
#define USB_STAT_PORT          GPIOF
#define USB_STAT_PIN           GPIO_Pin_1
#endif

#define USB_STAT_CHARGING      (GPIO_ReadInputDataBit(USB_STAT_PORT, USB_STAT_PIN)==Bit_RESET)

/*=======================================================================
          LED管脚
=========================================================================*/
#define RED_PORT               GPIOB
#define RED_PIN                GPIO_Pin_6
#define GREEN_PORT             GPIOB
#define GREEN_PIN              GPIO_Pin_7

#define RED_ON                 GPIO_WriteBit(RED_PORT, RED_PIN, Bit_RESET)
#define RED_OFF                GPIO_WriteBit(RED_PORT, RED_PIN, Bit_SET)
#define GREEN_ON               GPIO_WriteBit(GREEN_PORT, GREEN_PIN, Bit_RESET)
#define GREEN_OFF              GPIO_WriteBit(GREEN_PORT, GREEN_PIN, Bit_SET)

#define LIGHT_RED()            do{RED_ON;GREEN_OFF;}while(0)
#define LIGHT_YELLOW()         do{RED_ON;GREEN_ON;}while(0)
#define LIGHT_GREEN()          do{RED_OFF;GREEN_ON;}while(0)
#define DARK_RED_GREEN()       do{RED_OFF;GREEN_OFF;}while(0)

#define LED1_PORT              GPIOA
#define LED1_PIN               GPIO_Pin_6 
#define LED1_ON                GPIO_WriteBit(LED1_PORT, LED1_PIN, Bit_RESET)
#define LED1_OFF               GPIO_WriteBit(LED1_PORT, LED1_PIN, Bit_SET)

#define LED2_PORT              GPIOA
#define LED2_PIN               GPIO_Pin_5 
#define LED2_ON                GPIO_WriteBit(LED2_PORT, LED2_PIN, Bit_RESET)
#define LED2_OFF               GPIO_WriteBit(LED2_PORT, LED2_PIN, Bit_SET)

#define LED3_PORT              GPIOA
#define LED3_PIN               GPIO_Pin_4 
#define LED3_ON                GPIO_WriteBit(LED3_PORT, LED3_PIN, Bit_RESET)
#define LED3_OFF               GPIO_WriteBit(LED3_PORT, LED3_PIN, Bit_SET)

#define LED4_PORT              GPIOB
#define LED4_PIN               GPIO_Pin_3 
#define LED4_ON                GPIO_WriteBit(LED4_PORT, LED4_PIN, Bit_RESET)
#define LED4_OFF               GPIO_WriteBit(LED4_PORT, LED4_PIN, Bit_SET)


#define LED12345_OFF           do{LED1_OFF;LED2_OFF;LED3_OFF;LED4_OFF;}while(0)
/*=======================================================================
          DEBUG管脚
=========================================================================*/
#define DEBUG_PORT               GPIOF
#define DEBUG_PIN                GPIO_Pin_3
#define DEBUG_PIN_SOURCE         GPIO_PinSource3 

#define DEBUG_HIGH()             GPIO_WriteBit(RED_PORT, RED_PIN, Bit_SET)
#define DEBUG_LOW()              GPIO_WriteBit(RED_PORT, RED_PIN, Bit_RESET)

                                  
/*=======================================================================
          控制切换
-----------GPIO/PWM控制--------------------------------------------------
          上桥(P管) - GPIO控制
          下桥(N管) - PWM控制
=========================================================================*/

#if(CONTROL_TYPE==CONTROL_BY_GPIO)     
	/* P管 */
  #define OPEN_U_H    GPIO_WriteBit(PWM_CH1_PORT, PWM_CH1_PIN, GH_SET)
  #define CLOSE_U_H   GPIO_WriteBit(PWM_CH1_PORT, PWM_CH1_PIN, GH_RESET)
  #define OPEN_V_H    GPIO_WriteBit(PWM_CH2_PORT, PWM_CH2_PIN, GH_SET)
  #define CLOSE_V_H   GPIO_WriteBit(PWM_CH2_PORT, PWM_CH2_PIN, GH_RESET)
  #define OPEN_W_H    GPIO_WriteBit(PWM_CH3_PORT, PWM_CH3_PIN, GH_SET)
  #define CLOSE_W_H   GPIO_WriteBit(PWM_CH3_PORT, PWM_CH3_PIN, GH_RESET)
	#define DISABLE_UVW_H do{CLOSE_U_H;CLOSE_V_H;CLOSE_W_H;}while(0)   //关闭所有UVW_H输出
  
  /* N管 */
  #define OPEN_U_L    GPIO_WriteBit(PWM_CH1N_PORT, PWM_CH1N_PIN, GL_SET)
  #define CLOSE_U_L   GPIO_WriteBit(PWM_CH1N_PORT, PWM_CH1N_PIN, GL_RESET)
  #define OPEN_V_L    GPIO_WriteBit(PWM_CH2N_PORT, PWM_CH2N_PIN, GL_SET)
  #define CLOSE_V_L   GPIO_WriteBit(PWM_CH2N_PORT, PWM_CH2N_PIN, GL_RESET)
  #define OPEN_W_L    GPIO_WriteBit(PWM_CH3N_PORT, PWM_CH3N_PIN, GL_SET)
  #define CLOSE_W_L   GPIO_WriteBit(PWM_CH3N_PORT, PWM_CH3N_PIN, GL_RESET)
	#define DISABLE_UVW_L do{CLOSE_U_L;CLOSE_V_L;CLOSE_W_L;}while(0)  //关闭所有UVW_L输出

#elif(CONTROL_TYPE == CONTROL_BY_PWM_GPIO)     
  /* P管
     030 是TIM1.CH1，TIM1.CH2，TIM1.CH3
  */
  #define OPEN_U_H    TIM_CCxCmd(TIM1, TIM_Channel_1, TIM_CCx_Enable)   //PA1 - TIM1-CH1
  #define CLOSE_U_H   TIM_CCxCmd(TIM1, TIM_Channel_1, TIM_CCx_Disable)
  #define OPEN_V_H    TIM_CCxCmd(TIM1, TIM_Channel_2, TIM_CCx_Enable)   //PA2 - TIM1-CH2
  #define CLOSE_V_H   TIM_CCxCmd(TIM1, TIM_Channel_2, TIM_CCx_Disable)
  #define OPEN_W_H    TIM_CCxCmd(TIM1, TIM_Channel_3, TIM_CCx_Enable)   //PA3 - TIM1-CH3
  #define CLOSE_W_H   TIM_CCxCmd(TIM1, TIM_Channel_3, TIM_CCx_Disable)
	#define DISABLE_UVW_H TIM_CtrlPWMOutputs(TIM1, DISABLE)        //关闭所有UVW_L输出

	/* N管 */
  #define OPEN_U_L    GPIO_WriteBit(PWM_CH1N_PORT, PWM_CH1N_PIN, GL_SET)
  #define CLOSE_U_L   GPIO_WriteBit(PWM_CH1N_PORT, PWM_CH1N_PIN, GL_RESET)
  #define OPEN_V_L    GPIO_WriteBit(PWM_CH2N_PORT, PWM_CH2N_PIN, GL_SET)
  #define CLOSE_V_L   GPIO_WriteBit(PWM_CH2N_PORT, PWM_CH2N_PIN, GL_RESET)
  #define OPEN_W_L    GPIO_WriteBit(PWM_CH3N_PORT, PWM_CH3N_PIN, GL_SET)
  #define CLOSE_W_L   GPIO_WriteBit(PWM_CH3N_PORT, PWM_CH3N_PIN, GL_RESET)
	#define DISABLE_UVW_L do{CLOSE_U_L;CLOSE_V_L;CLOSE_W_L;}while(0)  //关闭所有UVW_L输出

#elif(CONTROL_TYPE == CONTROL_BY_GPIO_PWM)
	/* P管 */
  #define OPEN_U_H    GPIO_WriteBit(PWM_CH1_PORT, PWM_CH1_PIN, PH_SET)  //PA1
  #define CLOSE_U_H   GPIO_WriteBit(PWM_CH1_PORT, PWM_CH1_PIN, PH_RESET)
  #define OPEN_V_H    GPIO_WriteBit(PWM_CH2_PORT, PWM_CH2_PIN, PH_SET)
  #define CLOSE_V_H   GPIO_WriteBit(PWM_CH2_PORT, PWM_CH2_PIN, PH_RESET)
  #define OPEN_W_H    GPIO_WriteBit(PWM_CH3_PORT, PWM_CH3_PIN, PH_SET)
  #define CLOSE_W_H   GPIO_WriteBit(PWM_CH3_PORT, PWM_CH3_PIN, PH_RESET)
	#define DISABLE_UVW_H do{CLOSE_U_H;CLOSE_V_H;CLOSE_W_H;}while(0)   //关闭所有UVW_H输出
  
  /* N管 */
  #define OPEN_U_L    TIM_CCxCmd(TIM1, TIM_Channel_1, TIM_CCx_Enable)   //PD1 - TIM1-CH1
  #define CLOSE_U_L   TIM_CCxCmd(TIM1, TIM_Channel_1, TIM_CCx_Disable)
  #define OPEN_V_L    TIM_CCxCmd(TIM1, TIM_Channel_2, TIM_CCx_Enable)
  #define CLOSE_V_L   TIM_CCxCmd(TIM1, TIM_Channel_2, TIM_CCx_Disable)
  #define OPEN_W_L    TIM_CCxCmd(TIM1, TIM_Channel_3, TIM_CCx_Enable)
  #define CLOSE_W_L   TIM_CCxCmd(TIM1, TIM_Channel_3, TIM_CCx_Disable)
	#define DISABLE_UVW_L TIM_CtrlPWMOutputs(TIM1, DISABLE)        //关闭所有UVW_L输出
#endif

#endif

