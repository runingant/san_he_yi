#ifndef __square_wave_hardware_h__
#define __square_wave_hardware_h__

#include <stdint.h>
#include "sw_tools.h"
#include "hk32f0xx.h"   //包含CPU库文件
#include "hk32f0xx_gpio.h"

/*=======================================================================
          配置
=========================================================================*/
/* 板子选择 */
#define BOARD_FOC              (1)
#define BOARD_SQUARE_WAVE      (2)
#define BOARD_SQUARE_WAVE_II   (3)
#define BOARD_SQUARE_WAVE_III  (4)
#define BOARD_SQUARE_WAVE_105  (5)
#define BOARD_YI_RUI_DE        (6)      //毅瑞德驱动板
#define BOARD_GUO_MENG         (7)      //国梦驱动板
#define BOARD_SAN_HE_YI        (8)      //三合一驱动板
#define BOARD_SAN_HE_YI_V1_2   (9)      //三合一驱动板_V1.2
#define BOARD                  BOARD_SAN_HE_YI_V1_2

/* 电机选择 */
#define MOTOR_COMM_12GROOVE    (1)     //公模12槽电机
#define MOTOR_COMM_9GROOVE     (2)     //公模9槽电机     
#define MOTOR_YI_RUI_DE        (3)     //毅瑞德长轴承电机
#define MOTOR_GUO_MENG         (4)     //国梦电机（√）
#define MOTOR_YI_RUI_DE2       (5)     //毅瑞德端轴承电机（样机拆卸）
#define MOTOR_38COMM_12GROOVE  (6)     //38-12槽14对极电机
#define MOTOR                  MOTOR_38COMM_12GROOVE

/* 导通方式 */
#define LIANG_LIANG            2
#define SAN_SAN                3
#define DRIVE_MODE             LIANG_LIANG
  
#if (BOARD == BOARD_SQUARE_WAVE_II)
  #include "square_wave_hardware_II.h"
#elif (BOARD == BOARD_SQUARE_WAVE_III)
  #include "square_wave_hardware_III.h"
#elif (BOARD == BOARD_FOC)
  #include "square_wave_hardware_foc.h"
#elif ((BOARD == BOARD_SQUARE_WAVE_105) || (BOARD == BOARD_YI_RUI_DE) || (BOARD == BOARD_GUO_MENG))
  #include "square_wave_hardware_105.h"
#elif ((BOARD == BOARD_SAN_HE_YI) || (BOARD == BOARD_SAN_HE_YI_V1_2))
	#include "square_ware_hareware_shy.h"
#endif

#define BIAO_DING              (0)     //1-标定状态 0-正常状态

/* HALL采样方式选择 */
#define HALL_MODE_TIMER2       (1)
#define HALL_MODE              HALL_MODE_TIMER2       

/* 系统时钟选择 */
#define SYSTEM_CLOCK_M         (48)                     //参数24、48，单位M

/* TEST */
#define DEFAULT_PWM_PERCENT    (0.4)                    //默认PWM占空比

/*=======================================================================
          系统宏定义
=========================================================================*/
#if(SYSTEM_CLOCK_M == 24)
  #define SYSTEM_CLOCK         (24000000)
#elif(SYSTEM_CLOCK_M == 48)
  #define SYSTEM_CLOCK         (48000000)
#endif

/*=======================================================================
          PWM 硬件端口定义
          使用TIMER 1
=========================================================================*/
#define ENABLE_PWM_PORT_CLK()  do{ RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOA, ENABLE );\
                                 RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOB, ENABLE );\
                                 RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOC, ENABLE );\
                                 RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOD, ENABLE );\
}while(0)

#define PWM_TIMER                  TIM1
#define PWM_FREQ                   20000//25000 //2k  50K
#define PWMTIM_CLOCK_DIVIDER       1 
#define PWMTIM_PERIOD_CYCLE        (SYSTEM_CLOCK/PWM_FREQ) // = 48000000/2000 = 24000


#define STARTUP_PWM_MAX_VAL        (PWMTIM_PERIOD_CYCLE*0.45)  //启动阶段PWM最大值
#define RUNING_PWM_MAX_VAL         (PWMTIM_PERIOD_CYCLE*0.95)  //运行阶段PWM最大值

#define SET_TIM_CCR(x)             do{TIM1->CCR1 = x;TIM1->CCR2=x;TIM1->CCR3=x;}while(0)

#if(CONTROL_TYPE == CONTROL_BY_PWM_GPIO)
	#define TIM1_DEFAULT_CCR1        (PWMTIM_PERIOD_CYCLE)
#elif(CONTROL_TYPE == CONTROL_BY_GPIO_PWM)
	#define TIM1_DEFAULT_CCR1        (0)
#elif(CONTROL_TYPE == CONTROL_BY_GPIO)
  #define TIM1_DEFAULT_CCR1        (0)
#endif

/*=======================================================================
          HALL 硬件端口定义
          30,000rpm -> 5000转/秒 -> 如果是10对极，则50000次/秒，即每两次之间时间是0.02ms
          60rpm     -> 1转/秒 -> 最低1对极，极1次/秒，每次之间隔是1000ms
          要求测量timer能够支持0.02ms - 1000ms的测量
          因为timer值的范围是0-65535

          【系统时钟24M】
           分频系数_________最小计数时间_______最大计数时间
              0              0.0000416 us        2.73  us
              128            0.0053333 ms        0.349 s
              512            0.0213333 ms        1.398 s     [√]
              1024           0.0426666 ms        2.796 s
               
          【结论】选择1024分频能够基本满足要求
=========================================================================*/

#if (MOTOR == MOTOR_COMM_9GROOVE)
#define HALL_PAIRS                (6) //极对数  9槽电机6对级
#elif(MOTOR == MOTOR_38COMM_12GROOVE)
#define HALL_PAIRS                (7) //极对数  38电机7对极
#else
#define HALL_PAIRS                (5) //极对数  其他店家5对级
#endif
#define MEASURE_TIMER             TIM6
#define MEASURE_TIMER_PRESCALER   512
#define CALC_SPEED_FACTOR         (60/HALL_PAIRS) //60秒 / 极对数
#if(SYSTEM_CLOCK_M == 24)
#define MEASURE_TIMER_UNIT        0.00002133 //(秒)  24M
#elif(SYSTEM_CLOCK_M == 48)
#define MEASURE_TIMER_UNIT        0.00001066  //(秒) 48M
#endif


/*=======================================================================
          ADC 硬件端口定义
=========================================================================*/
#define ADCX                      ADC1
#define ENABLE_ADC_GPIO_CLK()  do{ RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOA, ENABLE );\
                                   RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOB, ENABLE );\
                                   RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOC, ENABLE );\
                                   RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOD, ENABLE );\
}while(0)

/*=======================================================================
          UART端口定义
=========================================================================*/
#define UART_TX_PORT        GPIOA
#define UART_TX_PIN         GPIO_Pin_2    //与SWDIO冲突，端口复用
#define UART_TX_PIN_SOURCE  GPIO_PinSource2


/*=======================================================================
          启停按键
=========================================================================*/
#define START_STOP_PORT          GPIOC
#define START_STOP_PIN           GPIO_Pin_13   
#define START_STOP_CLK_ENABLE()  do{RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOA, ENABLE );}while(0)

/*=======================================================================
          正反转
=========================================================================*/
#define MOTOR_DIR_PORT           GPIOC
#define MOTOR_DIR_PIN            GPIO_Pin_5 
#define MOTOR_DIR_SOURCE         GPIO_PinSource5 


/*=======================================================================
          计算
=========================================================================*/
#define CPU_VOLTAGE            (3.3)
#define VBUS_R_SAMPLE          (1)
#define VBUS_R_TOTAL           (2)
#define CURRENT_R              (0.02)  //电流采样电阻0.02Ω
#define ADC_CALC_FACTOR        (CPU_VOLTAGE / 4096)
/*=======================================================================
          函数说明
=========================================================================*/

/* 初始化CPU */
void drv_init_cpu(void);

/* 初始化NVIC中断 */
void drv_init_nvic(void);

/* 初始化PWM相关硬件 */
void drv_init_pwm(void);

/* 初始化adc */
void drv_init_adc(void);
void drv_init_adc1(void);

/* 初始化HALL硬件 */
void drv_init_hall(void);

/* 初始化方向检测 */
void drv_init_direction(void);

/* 初始化串口uart1 */
void drv_init_uart1(void);

void drv_init_gpio(void);

/* 初始化PWM测量硬件 */
void drv_init_pwm_capture(void);

/* 获取HALL状态 */
uint8_t drv_get_hallstate(void);

/* 获取temprature的adc值 */
uint32_t drv_get_regulation_adc(uint32_t *pVal1, uint32_t *pVal2, uint32_t*pVal3);



#define drv_get_direction()    GPIO_ReadInputDataBit(MOTOR_DIR_PORT, MOTOR_DIR_PIN)  //获取方向

void drv_start_measureTimer(void);
void drv_init_measureTimer(void);
uint32_t drv_get_measureTimer(void);
#endif
