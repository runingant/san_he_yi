#ifndef __square_wave_control_h__
#define __square_wave_control_h__

#include "square_wave_hardware.h"
#include "sw_tools.h"

/*=======================================================================
          版本定义
[产品线]_[硬件版本]_[发布版本]

-------------------------------------------------------------------------
【产品线】
  0 - 方波驱动板
  1 - 三合一板
-------------------------------------------------------------------------
【硬件版本】

1.方波驱动板硬件版本：

2.三合一板硬件版本：
   0 - V1.0硬件      
   1 - V1.1硬件
   2 - V1.2硬件
-------------------------------------------------------------------------
【发布版本】
   数字+t，举例"1t"  第1版本的测试版
   数字，举例"2", 发布版本2
=========================================================================*/
#define SOFT_VERSION    1.1.0  

/*=======================================================================
          参数宏定义
=========================================================================*/
#define OVER_CURRENT_HAPPEN_THRESHOLD       10000     //过流判断条件
#define OVER_CURRENT_DISAPPEAR_THRESHOLD    6000     //过流消失条件
#define OVER_CURRENT_LIMIT                  1500     //过流后，电流维持在1.5A，如果电流<1.5A，则回复
#define MOTOR_MAX_SPEED                     3200     //电机最大转速

/*=======================================================================
          状态定义
=========================================================================*/
typedef enum
{
  STA_POWER_ON    = 0,        //上电状态
  STA_START_UP    = 1,        //启动状态
  STA_RUNING      = 2,        //运行状态
  STA_OVER_CURRENT = 3,       //过流状态
  STA_PROTECT     = 5,
  STA_STOP        = 6,
}STATUS_E;

/* 运行模式 */
typedef enum
{
	MODE_AUTO=0,
	MODE_PWM,
}SW_MODE_E;

typedef struct sample_t
{
	float    fInputPwmPercent;   //输入PWM百分比
	uint32_t uCurrentMA;         //采样电流mA
	float    uVBus;              //Vbus电压值
}SAMPLE_T;

/*=======================================================================
          控制结构体
=========================================================================*/
typedef struct sw_control_s
{
	/* 运行状态 */
  uint16_t controlMode;    //控制模式  0-开环  1-自主闭环
  
  STATUS_E status;
  
	/* 采样数据 */
	SAMPLE_T sample;

	/* 运动控制 */
	uint8_t hallState;       //HALL当前状态
	uint8_t prevHallState;   //上一个HALL状态
	
  uint16_t targetSpeed;    //目标速度
  uint16_t nowSpeed;       //当前速度
  uint16_t speedCounter;   //速度计数器，用于计算速度
  uint16_t speedCounterPrev; //上一次有效的速度计数器
  
  
  uint32_t speedValidCounter; //速度环计数器
  uint32_t speedReliable;     //速度可靠标志
  
  int32_t pwmValue;          //PWM值
}SW_CONTROL_S;

/*=======================================================================
          参数声明
=========================================================================*/
extern SW_CONTROL_S sControl;
extern int16_t s_pwmMaxValue;
extern int16_t s_pwmMinValue;

#define LIMIT_MAX(max)                       (max > s_pwmMaxValue) ? s_pwmMaxValue:max   
#define LIMIT_MIN(min)                       (min < s_pwmMinValue) ? s_pwmMinValue:min
#define SET_TIM1_OUTPUT_PWM_MAX_VAL(val)     do{s_pwmMaxValue = val;}while(0)             //设置TIMER1输出PWM的最大值

/* 输出设置 */
#define ENABLE_ALL_DRIVER()    do{\
                                  TIM_Cmd(TIM1, ENABLE);\
                                  TIM_Cmd(TIM3, ENABLE);\
                                  TIM_ITConfig(TIM3,TIM_IT_CC1,ENABLE);\
                                  }while(0)
                                  
#define DISABLE_ALL_DRIVER()    do{\
																	SET_TIM_CCR(TIM1_DEFAULT_CCR1);\
																	TIM_Cmd(TIM1, DISABLE);\
                                  TIM_Cmd(TIM3, DISABLE);\
                                  TIM_ITConfig(TIM3,TIM_IT_CC1,DISABLE);\
                                  delay_ms(10);\
                                  DISABLE_UVW_H;\
                                  CLOSE_U_H; CLOSE_V_H; CLOSE_W_H;CLOSE_U_L; CLOSE_V_L; CLOSE_W_L;\
                                  }while(0)

/*=======================================================================
          运转逻辑
=========================================================================*/
#define SW_DELAY()      __nop()
#define OUTPUT_BA()     do{CLOSE_U_H;/*CLOSE_U_L;*//*CLOSE_V_H;*/CLOSE_V_L;CLOSE_W_H;CLOSE_W_L;\
                           OPEN_U_L;      /* A- */\
                           SW_DELAY();            \
                           OPEN_V_H;      /* B+ */\
                           TIM_GenerateEvent(TIM1, TIM_EventSource_COM);/*触发COM中断，立即更新PWM输出*/\
                          }while(0)

#define OUTPUT_CA()     do{CLOSE_U_H;/*CLOSE_U_L;*/CLOSE_V_H;CLOSE_V_L;/*CLOSE_W_H;*/CLOSE_W_L;\
                           OPEN_U_L;      /* A- */\
                           SW_DELAY();            \
                           OPEN_W_H;      /* C+ */\
                           TIM_GenerateEvent(TIM1, TIM_EventSource_COM);\
                          }while(0)
#define OUTPUT_CB()     do{CLOSE_U_H;CLOSE_U_L;CLOSE_V_H;/*CLOSE_V_L;*//*CLOSE_W_H*/;CLOSE_W_L;\
                           OPEN_V_L;      /* B- */\
                           SW_DELAY();            \
                           OPEN_W_H;      /* C+ */\
                           TIM_GenerateEvent(TIM1, TIM_EventSource_COM);\
                          }while(0)
#define OUTPUT_AB()     do{/*CLOSE_U_H;*/CLOSE_U_L;CLOSE_V_H;/*CLOSE_V_L;*/CLOSE_W_H;CLOSE_W_L;\
                           OPEN_V_L;      /* B- */\
                           SW_DELAY();            \
                           OPEN_U_H;      /* A+ */\
                           TIM_GenerateEvent(TIM1, TIM_EventSource_COM);\
                          }while(0)
#define OUTPUT_AC()     do{/*CLOSE_U_H;*/CLOSE_U_L;CLOSE_V_H;CLOSE_V_L;CLOSE_W_H;/*CLOSE_W_L;*/\
                           OPEN_W_L;      /* C- */\
                           SW_DELAY();            \
                           OPEN_U_H;      /* A+ */\
                           TIM_GenerateEvent(TIM1, TIM_EventSource_COM);\
                          }while(0)
#define OUTPUT_BC()     do{CLOSE_U_H;CLOSE_U_L;/*CLOSE_V_H;*/CLOSE_V_L;CLOSE_W_H;/*CLOSE_W_L;*/\
                           OPEN_W_L;      /* C- */\
                           SW_DELAY();            \
                           OPEN_V_H;      /* A+ */\
                           TIM_GenerateEvent(TIM1, TIM_EventSource_COM);\
                          }while(0)  

#define OUTPUT_AB_c()   do{CLOSE_U_H;CLOSE_U_L;CLOSE_V_H;CLOSE_V_L;CLOSE_W_H;CLOSE_W_L;\
                           SW_DELAY();\
                           OPEN_U_H;OPEN_V_H;\
                           OPEN_W_L;\
                           TIM_GenerateEvent(TIM1, TIM_EventSource_COM);\
                          }while(0) 
#define OUTPUT_B_ac()   do{CLOSE_U_H;CLOSE_U_L;CLOSE_V_H;CLOSE_V_L;CLOSE_W_H;CLOSE_W_L;\
                           SW_DELAY();\
                           OPEN_V_H;\
                           OPEN_U_L;OPEN_W_L;\
                           TIM_GenerateEvent(TIM1, TIM_EventSource_COM);\
                          }while(0) 
#define OUTPUT_BC_a()   do{CLOSE_U_H;CLOSE_U_L;CLOSE_V_H;CLOSE_V_L;CLOSE_W_H;CLOSE_W_L;\
                           SW_DELAY();\
                           OPEN_V_H;OPEN_W_H;\
                           OPEN_U_L;\
                           TIM_GenerateEvent(TIM1, TIM_EventSource_COM);\
                          }while(0) 
#define OUTPUT_C_ab()   do{CLOSE_U_H;CLOSE_U_L;CLOSE_V_H;CLOSE_V_L;CLOSE_W_H;CLOSE_W_L;\
                           SW_DELAY();\
                           OPEN_W_H;\
                           OPEN_U_L;OPEN_V_L;\
                           TIM_GenerateEvent(TIM1, TIM_EventSource_COM);\
                          }while(0) 
#define OUTPUT_AC_b()   do{CLOSE_U_H;CLOSE_U_L;CLOSE_V_H;CLOSE_V_L;CLOSE_W_H;CLOSE_W_L;\
                           SW_DELAY();\
                           OPEN_U_H;OPEN_W_H;\
                           OPEN_V_L;\
                           TIM_GenerateEvent(TIM1, TIM_EventSource_COM);\
                          }while(0) 
#define OUTPUT_A_bc()   do{CLOSE_U_H;CLOSE_U_L;CLOSE_V_H;CLOSE_V_L;CLOSE_W_H;CLOSE_W_L;\
                           SW_DELAY();\
                           OPEN_U_H;\
                           OPEN_V_L;OPEN_W_L;\
                           TIM_GenerateEvent(TIM1, TIM_EventSource_COM);\
                          }while(0) 

#define CLOSE_ABC()    do{CLOSE_U_H;CLOSE_U_L;CLOSE_V_H;CLOSE_V_L;CLOSE_W_H;CLOSE_W_L;}while(0)


/*=======================================================================
          函数声明
=========================================================================*/
void sw_calc_speed(void);//计算速度
void sw_hall_switch(SW_CONTROL_S *pContorl, uint8_t force);//根据HALL切换输出
void sw_auto_switch(void);//自动切换输出

void set_overcurrent_event(uint8_t state); //设置过流事件
uint16_t is_overcurrent_happen(void);//判断过流事件发生

void set_flash_protection(void);//设置flash保护
void set_control_mode(SW_CONTROL_S* pControl, uint8_t mode);//设置运行模式

void current_limit_output(SW_CONTROL_S *pControl, uint16_t current);//限流输出
void pwm_mode_output(SW_CONTROL_S *pControl);//输入PWM模式下输出
void auto_mode_output(SW_CONTROL_S *pControl);//自动模式下输出
void standby_output(SW_CONTROL_S *pControl);//

bool check_output_enable(SW_CONTROL_S *pControl);//检查输出使能

void sw_calculate_speed(SW_CONTROL_S *pControl);
void sw_calc_speed_by_cycle(void);
void drv_init_debug(void);
void sw_init_hardware(void);
void print_motor_info(void);

float get_vbat_voltage(void);
float get_motor_current(void);
void set_input_pwm_percent(float percent);

extern void sw_status(SW_CONTROL_S *pControl);
extern void platform_init(void);

#endif
