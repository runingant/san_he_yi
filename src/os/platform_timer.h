
#ifndef __PLATFORM_TIMER_H__
#define __PLATFORM_TIMER_H__

#include "global.h"

#define TIMER_NUMBER    5

typedef enum
{
    TIMER_IDLE=0,
    TIMER_ONCE,         //就运行一次 
    TIMER_ONE_BY_ONE,   //逐次运行(即其回调函数处理完成之后再开始新定时)
    TIMER_CONTINUE      //连续进行(即其定时是连续不断进行的)
}TIMER_E;
typedef struct
{
    TIMER_E   run_en;
    uint8_t   state;        //当前状态(0-上电状态、1-按键按下、2-按键抬起、3-应用使用,代表按键已经被处理)
    uint16_t  delay_ms;     //延时值
    uint16_t  reload_ms;    //重载值(间隔时间)
    pTaskHook hook;         //回调函数指针
}TIMER_T;

void pt_regist_hardware(void);
void pt_timer_init(void);
void pt_timer_service(void);
bool pt_timer_delete_all(void);
bool pt_timer_add(TIMER_E en, uint8_t index, uint16_t dlyms, pTaskHook pfunc);
bool pt_timer_delete(uint8_t index);


extern pTaskHook pHook_timer;
#define __set_HookTimer(func)     do{pHook_timer = func;}while(0)
#define __get_HookTimer()         pHook_timer;
#endif
