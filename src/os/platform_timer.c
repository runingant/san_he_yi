/*平台层-时间相关
任务运转服务-时间处理
*/

#include <string.h>
#include "platform_timer.h"
//#include "stm32f10x.h"


/*static*/ TIMER_T app_timer[TIMER_NUMBER];

static void pt_timer_dec(void);

uint8_t irq_state = 0;  //test
#define cpu_irq_disable()   __nop()//do{__set_PRIMASK(1);irq_state=0;}while(0)
#define cpu_irq_enable()    __nop()//do{__set_PRIMASK(0);irq_state=1;}while(0)

/**************************************************************
* 函 数 名: pt_regist_hardware()
* 功    能: 调用底层接口，注册(即将pt_timer_dec()加入TIMER中断中)
* 输    入:
* 返    回: 
* 其    他: 
-------------------------------------------------------------
* 日    期: 2016-04-25
**************************************************************/
void pt_regist_hardware(void)
{
  __set_HookTimer(pt_timer_dec);
}


/**************************************************************
* 函 数 名: pt_timer_init()
* 功    能: timer初始化
* 输    入:
* 返    回: 
* 其    他: 
-------------------------------------------------------------
* 日    期: 2016-04-25
**************************************************************/
void pt_timer_init(void)
{
  for(uint8_t i=0; i<TIMER_NUMBER; i++)
  {
    memset((uint8_t *)&app_timer[i], 0x00, sizeof(TIMER_T));
  }
}

/**************************************************************
* 函 数 名: pt_timer_dec()
* 功    能: timer处理，由中断调用
* 输    入:
* 返    回: 
* 其    他: 
-------------------------------------------------------------
* 日    期: 2016-04-25
**************************************************************/
static void pt_timer_dec(void)
{
  uint8_t i=0;
  
  for(i=0; i<TIMER_NUMBER; i++)
  {
    if(app_timer[i].run_en)
    {
      if(app_timer[i].delay_ms)
      {
        app_timer[i].delay_ms--;
      }
      else
      {
        if(app_timer[i].run_en == TIMER_CONTINUE)
        {
          app_timer[i].delay_ms = app_timer[i].reload_ms;
        }
        
        if(app_timer[i].state == 0)
        {
            app_timer[i].state = 1; //任务已准备好运行
        }
      }
    }
  }
}

/**************************************************************
* 函 数 名: pt_timer_dec()
* 功    能: 定时服务，由主循环调用
* 输    入:
* 返    回: 
* 其    他: 
-------------------------------------------------------------
* 日    期: 2016-04-25
**************************************************************/
void pt_timer_service(void)
{
  uint8_t i=0;
    
  for(i=0; i<TIMER_NUMBER; i++)
  {
    if(app_timer[i].run_en)
    {
      if(app_timer[i].state)
      {   
        app_timer[i].hook();    //执行hook函数    
        
        cpu_irq_disable(); 
        app_timer[i].state = 0; 
        if(app_timer[i].run_en == TIMER_ONCE)
        {
            app_timer[i].run_en = TIMER_IDLE;   //单次运行则 清空
        }
        else if(app_timer[i].run_en == TIMER_ONE_BY_ONE)                     
        {
            app_timer[i].delay_ms = app_timer[i].reload_ms; //逐次运行则 hook处理完成后再重载
        }
        else
        {}
        cpu_irq_enable();
      }    
    }
  }
}

/**************************************************************
* 函 数 名: pt_timer_add()
* 功    能: 添加定时服务
* 输    入:
* 返    回: 
* 其    他: 
-------------------------------------------------------------
* 日    期: 2016-04-25
**************************************************************/
bool pt_timer_add(TIMER_E en, uint8_t index, uint16_t dlyms, pTaskHook pfunc)
{
    if((app_timer[index].run_en) || (index >= TIMER_NUMBER))
    {
        return false;
    }
    
    if((en == TIMER_IDLE) || (en > TIMER_CONTINUE))
    {
        return false;
    }
    
    app_timer[index].reload_ms = dlyms;
    app_timer[index].delay_ms = dlyms;
    app_timer[index].hook = pfunc;
    
    cpu_irq_disable();
    app_timer[index].run_en = en;
    cpu_irq_enable();
    
    return true;
}

/**************************************************************
* 函 数 名: pt_timer_delete()
* 功    能: 删除单个任务
* 输    入:
* 返    回: 
* 其    他: 
-------------------------------------------------------------
* 日    期: 2016-04-25
**************************************************************/
bool pt_timer_delete(uint8_t index)
{
    cpu_irq_disable();
    app_timer[index].run_en = TIMER_IDLE;
    cpu_irq_enable();
    app_timer[index].reload_ms = 0;
    app_timer[index].delay_ms = 0;
    app_timer[index].hook = NULL;
    return true;
}

/**************************************************************
* 函 数 名: pt_timer_delete_all()
* 功    能: 清空全部任务
* 输    入:
* 返    回: 
* 其    他: 
-------------------------------------------------------------
* 日    期: 2016-04-25
**************************************************************/
bool pt_timer_delete_all(void)
{
    uint8_t index=0;
    
    for(index=0; index<TIMER_NUMBER; index++)
    {
        cpu_irq_disable();
        app_timer[index].run_en = TIMER_IDLE;
        cpu_irq_enable();
        app_timer[index].reload_ms = 0;
        app_timer[index].delay_ms = 0;
        app_timer[index].hook = NULL;
    }
    return true;
}

