
#ifndef __platform_service_h__
#define __platform_service_h__

#include "global.h"
#include "platform_timer.h"
//#define feed_wdt()      IWDG_KR = 0xAA          //刷新IDDG，避免产生看门狗复位，同时恢复 PR 及 RLR 的写保护状态
//#define do_sleep()      do{feed_wdt();asm("halt");}while(0)             //执行休眠 

void platform_init(void);
void platform_server(void);
void platform_runHook(void);


bool platform_create_task(TIMER_E en, uint8_t index, uint16_t dlyms, pTaskHook pfunc);
bool platform_delete_task(uint8_t index);

#endif
