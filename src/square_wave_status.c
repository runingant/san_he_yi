#include "sw_tools.h"
#include "square_wave_hardware.h"
#include "square_wave_control.h"
#include "square_wave_sample.h"

/* 开环判定条件 */
//#define CHECK_INPUT_PWM_VALID(pControl)     ((pControl->sample.fInputPwmPercent < 0.90) && (pControl->sample.fInputPwmPercent >= 0))   //有效PWM条件
//#define CHECK_INPUT_PWM_INVALID(pControl)   (pControl->sample.fInputPwmPercent >= 0.98)  //无效PWM条件


/* 闭环判定条件 */
#define CHECK_INPUT_PWM_VALID(pControl)     ((pControl->sample.fInputPwmPercent < 0.90) && (pControl->sample.fInputPwmPercent >= 0.2))
#define CHECK_INPUT_PWM_INVALID(pControl)   (pControl->sample.fInputPwmPercent < 0.1)  //无效PWM条件

//#define CHECK_INPUT_PWM_VALID(pControl)     1//(pControl->sample.fInputPwmPercent >= 0.10)
//#define CHECK_INPUT_PWM_INVALID(pControl)   0//(pControl->sample.fInputPwmPercent < 0.10)
#define CHECK_START_SPEED_OK(pControl)      (pControl->nowSpeed > 600)                   //通过速度判断是否启动成功
                         
/* 状态切换 */
#define SW_SWITCH(pControl, sta)  do{pControl->status = sta;printf("\r\n sw = %d\r\n", sta);}while(0)    

/* 变量定义 */
static SYSTEM_TIME_T s_overcurrent_timer = {0}; //过流时间
static uint8_t       s_startUpStatus = 0;     //启动标志(0-未启动，1-已经启动)
static uint16_t      overCurrentTimes;        //过流次数

/**********************************************************************
 * @bref    启动状态
 * @param   pControl - 电机控制参数指针 
 **********************************************************************/
void start_up_state_process(SW_CONTROL_S *pControl)
{
  if(s_startUpStatus == 0)
  {
    SET_TIM1_OUTPUT_PWM_MAX_VAL(STARTUP_PWM_MAX_VAL);           //首次启动设置PWM最大输出值：80%
    //sw_hall_switch(pControl, 1);                                   //上电启动
    ENABLE_ALL_DRIVER();                                             //使能所有驱动
    sw_hall_switch(pControl, 1);                                     //上电启动
    TIM_ITConfig(TIM3,TIM_IT_CC1,ENABLE);
    s_startUpStatus = 1;
  }
  else
  {
		if(CHECK_INPUT_PWM_INVALID(pControl))           
	  {
	  	DISABLE_ALL_DRIVER();
	    SW_SWITCH(pControl, STA_STOP);                                //无PWM，切换STOP状态
	    s_startUpStatus = 0;
	    overCurrentTimes = 0;
	  }
    else if(CHECK_START_SPEED_OK(pControl))
    {
    	SET_TIM1_OUTPUT_PWM_MAX_VAL(RUNING_PWM_MAX_VAL);              //速度OK，则切换运行状态
      SW_SWITCH(pControl, STA_RUNING); 
			s_startUpStatus = 0;
			overCurrentTimes = 0;
    }
    else if(pControl->sample.uCurrentMA >= OVER_CURRENT_HAPPEN_THRESHOLD)
		{
			overCurrentTimes++;
			if(overCurrentTimes >= 30)
			{
				DISABLE_ALL_DRIVER();
				SW_SWITCH(pControl, STA_PROTECT);                             //电流超限，切换保护状态
				overCurrentTimes = 0;
				s_startUpStatus = 0;
			}
		}
		else
    {
      pwm_mode_output(pControl);                                    //输出PWM
    }
  }
}

/**********************************************************************
 * @bref    运行状态处理
 * @param   pControl - 电机控制参数指针 
   OVER_CURRENT_HAPPEN_THRESHOLD - 电流参数
 **********************************************************************/
uint8_t gOverCurrentHappen = 0;//test
void runing_state_process(SW_CONTROL_S *pControl)
{
	if(CHECK_INPUT_PWM_INVALID(pControl))
	{/* 保护 */
		DISABLE_ALL_DRIVER();
		SW_SWITCH(pControl, STA_STOP);  
	}
	else if(pControl->nowSpeed < 100)
	{/* 堵转保护 */
    if(gOverCurrentHappen == 0)
    {
      gOverCurrentHappen = 1;
      s_overcurrent_timer = get_systime(); //记录开始时间
    }
		current_limit_output(pControl, OVER_CURRENT_DISAPPEAR_THRESHOLD);
	}
	else
	{/* 限流输出 */
    gOverCurrentHappen = 0;
		if (pControl->sample.uCurrentMA < OVER_CURRENT_HAPPEN_THRESHOLD)
		{
			pwm_mode_output(pControl);
		}
		else
		{
			current_limit_output(pControl, OVER_CURRENT_HAPPEN_THRESHOLD); 
		}
	}
  
  /* 过流时间长，则切换保护状态 */
  if( gOverCurrentHappen && (compare_systime_ms(&s_overcurrent_timer) >= 1500) )
  {
    DISABLE_ALL_DRIVER();                          //关闭所有输出
    SW_SWITCH(pControl, STA_PROTECT); 
  }
}


/* 保护状态处理 
20200804:测试到有异常的百分比参数，在档位为1的情况下检测到百分比超过0.99。因此增加滤波变量
*/
static uint16_t s_inputPwmConfirmCnt = 0; //test
void protection_state_process(SW_CONTROL_S *pControl)
{
	uint32_t percent = pControl->sample.fInputPwmPercent * 100;
	
  if(CHECK_INPUT_PWM_INVALID(pControl))         //无PWM，切换STOP状态
  {
  	s_inputPwmConfirmCnt++;
  }
	else
	{
		s_inputPwmConfirmCnt = 0;
	}

	
	if(s_inputPwmConfirmCnt > 20)
	{
		s_inputPwmConfirmCnt = 0;
  	printf("switch to 5, percent = %d\r\n", percent);
  	DISABLE_ALL_DRIVER();
    SW_SWITCH(pControl, STA_STOP);
  }   
}

/* 停止状态处理 */
void stop_state_process(SW_CONTROL_S *pControl)
{
	static uint8_t s_stopState = 0;
	static SYSTEM_TIME_T s_stopTimer = {0};

	if(pControl->controlMode == MODE_AUTO)
	{/* 闭环模式延迟500ms */
		switch(s_stopState)
		{
			case 0:
				if(CHECK_INPUT_PWM_VALID(pControl))
				{
					s_stopState = 1;
					s_stopTimer = get_systime();
				}
				break;

			case 1:
				if(CHECK_INPUT_PWM_VALID(pControl))
				{
					if(compare_systime_ms(&s_stopTimer) > 100) //100ms
					{
						s_stopState = 0;
						SW_SWITCH(pControl, STA_START_UP);
					}
				}
				else
				{
					s_stopState = 0;
				}
				break;

			default:break;
		}
	}
	else if(pControl->controlMode == MODE_PWM)
	{/* 开环模式立即输出 */
		if(CHECK_INPUT_PWM_VALID(pControl))
		{
			SW_SWITCH(pControl, STA_START_UP);
		}
	}
}
/**********************************************************************
 * @bref	 主控状态机
 * @param  pControl - 控制指针
 **********************************************************************/
void sw_status(SW_CONTROL_S *pControl)
{
  switch(pControl->status)
  {
    case STA_POWER_ON:        /* 0 上电 */
    {
      if(CHECK_INPUT_PWM_VALID(pControl))
      {
      	s_startUpStatus = 0;
        SW_SWITCH(pControl, STA_STOP);
      }
      break;
    }
    
    case STA_START_UP:       /* 1 启动 */
    {
      start_up_state_process(pControl);
      break;
    }
    
    case STA_RUNING:        /* 2 运行 */
    {
      runing_state_process(pControl);
      break;
    }
    
    case STA_PROTECT:    /* 5 保护 */
    {
      protection_state_process(pControl);
      break;
    }
    
    case STA_STOP:      /* 6 停止 */
    {
      stop_state_process(pControl);
      break;
    }
    
    default:break;
  }
}
