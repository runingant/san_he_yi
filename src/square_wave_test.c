#include <stdio.h>
#include "hk32f0xx.h"
#include "sw_tools.h"
#include "platform_service.h" 
#include "square_wave_pid.h"
#include "square_wave_control.h"
#include "square_wave_sample.h"

/* 电流PID参数 */
PID sOverCurrentPid = {
	.Kp = 0.002,
	.Ki = 0.001,
	.Kd = 0.0,
};




void test_over_current_state(SW_CONTROL_S *pControl)
{
  int32_t pwm = 0;
  pwm = (int32_t)PIDRegulation(OVER_CURRENT_LIMIT, pControl->sample.uCurrentMA, &sOverCurrentPid); 
  pControl->pwmValue += pwm;

  /* 限幅处理并更新TIMER */
  pControl->pwmValue = LIMIT_MAX(pControl->pwmValue);
  pControl->pwmValue = LIMIT_MIN(pControl->pwmValue);
#if(CONTROL_TYPE == CONTROL_BY_PWM_GPIO)
		SET_TIM_CCR(PWMTIM_PERIOD_CYCLE - pControl->pwmValue);
#elif(CONTROL_TYPE == CONTROL_BY_GPIO_PWM)
		SET_TIM_CCR(pControl->pwmValue);
#endif
	printf("add=%d, pwm=%d, uCurrent=%d\r\n", pwm, pControl->pwmValue, pControl->sample.uCurrentMA);
}


void test_status(SW_CONTROL_S *pControl)
{
  switch(pControl->status)
  {
  	case STA_POWER_ON:
  	{
  		SET_TIM1_OUTPUT_PWM_MAX_VAL(STARTUP_PWM_MAX_VAL);
			OUTPUT_CB();
			pControl->status = STA_OVER_CURRENT;
  		break;
  	}
			
    case STA_OVER_CURRENT:/* 3 过流 */
    {
      test_over_current_state(pControl);  
      break;
    }
		
    default:break;
  }
}




