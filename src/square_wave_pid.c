#include "square_wave_pid.h"

/*=======================================================================
          PID工具函数
=========================================================================*/

/**********************************************************************
 * @bref    设置PID参数
 * @param  
 **********************************************************************/
void PIDSetPID(PID *vPID, float Kp, float Ki, float Kd)
{
  vPID->Kp = Kp;
  vPID->Ki = Ki;
  vPID->Kd = Kd;
}

/**********************************************************************
 * @bref    设置PID输出值
 * @param  
 **********************************************************************/
void PIDSetResult(PID *vPID, float result)
{
  vPID->result = result;
}  

/**********************************************************************
 * @bref    PID计算函数
 * @param  
 **********************************************************************/
float PIDRegulation(int16_t SetValue, int16_t ActualValue, PID *vPID)
{
  float thisError;
  float increment;
  float pError,dError,iError;
  
  thisError = SetValue - ActualValue; //当前误差等于设定值减去当前值
  //thisError = ActualValue - SetValue; //当前误差等于设定值减去当前值
  
  //计算公式中除系数外的三个 乘数
  pError=thisError;    
  iError=thisError - vPID->lasterror;//两次偏差差值err(k)-err(k-1)
  dError=thisError-2*(vPID->lasterror)+vPID->preerror;

  increment = vPID->Kp*pError + vPID->Ki*iError + vPID->Kd*dError;   //增量计算

  vPID->preerror=vPID->lasterror;  //存放偏差用于下次运算
  vPID->lasterror=thisError;

	return increment;
  
  //vPID->result += increment;//结果当然是上次结果 加上本次增量
}



